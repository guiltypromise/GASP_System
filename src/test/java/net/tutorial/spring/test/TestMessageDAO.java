package net.tutorial.spring.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import net.tutorial.spring.config.AppConfig;
import net.tutorial.spring.config.TestDAOConfig;
import net.tutorial.spring.dao.MessageDAO;
import net.tutorial.spring.model.Message;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { AppConfig.class, TestDAOConfig.class } )
@WebAppConfiguration
@Transactional
public class TestMessageDAO {
	
	@Autowired
	private MessageDAO messageDAO;
	
	private Message message;
	
	@Before
	public void setUp() throws Exception {
		message = new Message();
	}
	
	@After
	public void tearDown() throws Exception {
		message = null;
	}
	
	public void insertData() {
		
		message.setSenderName("TestSender");
		message.setReceiverName("TestRecipient");
		message.setSubject("TestSubject");
		message.setMessage("TestMessage TestMessage");
		assertTrue( messageDAO.createMessage(message) );
	}
	
	@Test
	public void testInsertMsgPositive() {
		message.setSenderName("TestSender");
		message.setReceiverName("TestRecipient");
		message.setSubject("TestSubject");
		message.setMessage("TestMessage TestMessage");
		assertTrue( messageDAO.createMessage(message) );
	}
	
	@Test
	public void testInsertMsgNegative() {
		Message message = new Message();
		assertFalse( messageDAO.createMessage(message) );
	}
	
	@Test
	public void testReadSentMessagesPositive() {
		insertData();
		assertFalse( messageDAO.readSentMessages("TestSender").isEmpty() );
	}
	
	@Test
	public void testReadSentMessagesNegative() {
		insertData();
		assertTrue( messageDAO.readSentMessages("TestSender1").isEmpty() );
	}
	@Test
	public void testReadRecvdMessagesPositive() {
		insertData();
		assertFalse( messageDAO.readReceivedMessages("TestRecipient").isEmpty() );
	}
	
	@Test
	public void testReadRecvdMessagesNegative() {
		insertData();
		assertTrue( messageDAO.readReceivedMessages("TestRecipient1").isEmpty() );
	}
	
}
