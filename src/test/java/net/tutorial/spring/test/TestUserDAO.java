/**
 * TestUserDAO.java
 * 
 * Unit component test for database operations on the User table
 * 
 * @author jason.r.martin
 */

package net.tutorial.spring.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.tutorial.spring.config.AppConfig;
import net.tutorial.spring.config.TestDAOConfig;
import net.tutorial.spring.dao.UserDAO;
import net.tutorial.spring.model.User;

@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { AppConfig.class, TestDAOConfig.class } )
@WebAppConfiguration
@Transactional
public class TestUserDAO {
	
	@Autowired
	private UserDAO userDAO;
	
	private User user;

	
	/**
	 * Setting up initial objects
	 *
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		user = new User();
	}
	
	/**
	 * Deallocating objects after execution of every method
	 *
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		user = null;
	}
	
	public void insertData() {
		user.setFirstName( "TestUserFirstName" );
		user.setLastName( "TestUserLastName" );
		user.setEmail( "TestUser@TestEmail.com" );
		user.setUsername( "TestUser" );
		user.setPassword( "TestPassword" );
		user.setEnabled( true );
		assertTrue( userDAO.insertUser(user) );
	}
	
	@Test
	public void testInsertUserPositive() {
		user.setFirstName( "TestUserFirstName" );
		user.setLastName( "TestUserLastName" );
		user.setEmail( "TestUser@TestEmail.com" );
		user.setUsername( "TestUser" );
		user.setPassword( "TestPassword" );
		assertTrue( userDAO.insertUser(user) );
	}
	
	@Test
	public void testInsertUserNegative() {
		insertData();
		user.setFirstName( "TestUserFirstName" );
		user.setLastName( "TestUserLastName" );
		user.setEmail( "TestUser@TestEmail.com" );
		user.setPassword( "TestPassword" );
		user.setUsername( "TestUser" );
		assertFalse( userDAO.insertUser(user) );
	}
	
	@Test
	public void testSearchUsernamePositive() {
		insertData();
		assertNotNull( userDAO.searchUser("TestUser") );
	}
	
	@Test
	public void testSearchUsernameNegative() {
		insertData();
		assertNull( userDAO.searchUser("TestUser1") );
	}
	
	@Test
	public void testSearchUserEmailPositive() {
		insertData();
		assertTrue( userDAO.searchUserEmail("TestUser@TestEmail.com") );
	}
	
	@Test
	public void testSearchUserEmailNegative() {
		insertData();
		assertFalse( userDAO.searchUserEmail("TestUser1@TestEmail.com") );
	}
	
	@Test
	public void testSearchUsernamePasswordPositive() {
		insertData();
		User dbUser = userDAO.searchUserWithPw("TestUser", "TestPassword" );
		assertNotNull(dbUser);
	}
	
	@Test 
	public void testSearchUsernamePasswordNegative() {
		insertData();
		User dbUser = userDAO.searchUserWithPw("TestUser1", "TestPassword" );
		assertNull( dbUser );
	}
	
	@Test 
	public void testListUsersPositive() {
		
		List<User> list = userDAO.listUsers();
		assertNotNull( list );
	}
	
	@Test
	public void testUpdateUserPositive() {
		insertData();
		User user = userDAO.searchUser("TestUser");
		user.setFirstName("TestUserFirstName1");
		assertTrue( userDAO.updateUser(user) );
	}
	
	@Test
	public void testUpdateUserNegative() {
		User user = new User();
		assertFalse( userDAO.updateUser(user) );
	}
	
	@Test
	public void testDeleteUserPositive() {
		insertData();
		User user = userDAO.searchUser("TestUser");
		assertTrue( userDAO.deleteUser(user) );
	}
	
	@Test
	public void testDeleteUserNegative() {
		User user = new User();
		assertFalse( userDAO.deleteUser(user) );
	}
}

