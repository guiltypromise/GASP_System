/**
 * UserRole.java
 * 
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.model;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//@Entity
//@Table(name = "user_roles", uniqueConstraints = @UniqueConstraint(columnNames = { "username" }))
public class UserRole{

//	@Id
//	@ManyToOne( fetch = FetchType.LAZY )
//	@JoinColumn( name = "username", nullable = false, unique = true )
	private User user;
	
	@Column( name = "role", nullable = false, length = 45 )
	private String role;
	
	public UserRole () {
		
	}
	
	//Many roles can be assigned to one User
	public User getUser() {
		return this.user;
	}
	
	public void setUser( User user ) {
		this.user = user;
	}
	
	public String getRole() {
		return this.role;
	}
	
	public void setRole( String role ) {
		this.role = role;
	}
}
