/**
 * User.java
 * 
 * 
 * @author jason.r.martin
 */

package net.tutorial.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "user" )
public class User {
	
	@Id
	@Column( name = "id" )
	@GeneratedValue(strategy=GenerationType.IDENTITY) // to agree with primary key auto-inc in MySQL table
	private int userID;
	
	@Column( name = "first_name", nullable = false )
	private String firstName;
	
	@Column( name = "last_name", nullable = false  )
	private String lastName;
	
	
	@Column( name = "email", nullable = false, unique = true )
	private String email;
	
	@Column( name = "username", nullable = false,  unique = true )
	private String username;
	
	@Column( name = "password", nullable = false  )
	private String password;
	
	@Column( name = "enabled", nullable = false )
	private boolean enabled;
	
	@Column( name = "role", nullable = false )
	private String role;
	
	@Column( name = "avatarPath", nullable = true )
	private String avatarPath;
															  

	public User() {
		firstName = "";
		username = "";
		password = "";
		role = "USER";
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public String getAvatarPath() {
		return avatarPath;
	}

	public void setAvatarPath(String avatarPath) {
		this.avatarPath = avatarPath;
	}
}
