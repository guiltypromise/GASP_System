/**
 * Message.java
 * 
 * @author jason.r.martin
 */

package net.tutorial.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "message" )
public class Message {
	
	@Id
	@Column( name = "id" )
	@GeneratedValue(strategy=GenerationType.IDENTITY) // to agree with primary key auto-inc in MySQL table
	private int messageID;
	


	@Column( name = "sender" )
	private String sender;

	@Column( name = "recipient" )
	private String recipient;
	
	@Column( name = "subject" )
	private String subject;
	
	@Column( name = "message" )
	private String message;
	
	@Column( name = "isNew" )
	private boolean isNew;
	
	@Column( name = "timestamp")
	private Date timestamp;

	public Message() {
		this.isNew = true; // all created messages are new until read
	}

	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	
	public String getSenderName() {
		return sender;
	}

	public void setSenderName(String senderName) {
		this.sender = senderName;
	}

	public String getReceiverName() {
		return this.recipient;
	}

	public void setReceiverName(String receiverName) {
		this.recipient = receiverName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}
