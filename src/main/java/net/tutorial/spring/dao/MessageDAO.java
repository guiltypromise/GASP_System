/**
 * MessageDAO.java
 * 
 * @author jason.r.martin
 */

package net.tutorial.spring.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.tutorial.spring.model.Message;

@Repository("messageDAO")
public class MessageDAO {
	
	private static final Logger LOGGER = Logger.getLogger(MessageDAO.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Grabs the session from Hibernate to perform persistence.
	 * @return
	 */
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	
	public boolean createMessage(Message message) {
		LOGGER.info("Preparing to send message into DB");
		
		LOGGER.info("Sender: " + message.getSenderName());
		LOGGER.info("Recipient: " + message.getReceiverName());
		LOGGER.info("Subject: " + message.getSubject());
		LOGGER.info("Message: " + message.getMessage());
		
		try {
			getSession().persist(message);
		} catch (HibernateException he ) {
			LOGGER.error( he );
			return false;
		}
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Message getMessageById( Message message ) {
		Message msg = null;
		
		Criteria cr = getSession().createCriteria( Message.class );
		cr.add( Restrictions.eq( "messageID", message.getMessageID() ) );
		
		LOGGER.debug("Retrieving message id: " + message.getMessageID() );
		
		List<Message> msgResult = cr.list();
		
		if( !msgResult.isEmpty() ) {
			msg = msgResult.get(0);
		} else {
			LOGGER.error( "Unable to retrieve message.");
		}
		
		return msg;
	}


	@SuppressWarnings("unchecked")
	public List<Message> readReceivedMessages( String recipientName ) {
		LOGGER.debug(" Entering readRecievedMessages()" );
		List<Message> msgs = null;

		Criteria cr = getSession().createCriteria( Message.class );
		cr.add( Restrictions.eq( "recipient", recipientName ) );
		try {
			msgs = cr.list();
			if( msgs.isEmpty() ) {
				return msgs;
			}

		} catch ( HibernateException he ) {
			LOGGER.error(he);
		}
		return msgs;
	}
	
	@SuppressWarnings("unchecked")
	public int countNewMessages( String recipientName ) {
		LOGGER.debug(" Entering readRecievedMessages()" );
		int count = 0;
		List<Message> msgs = null;
		Criteria cr = getSession().createCriteria( Message.class );
		cr.add( Restrictions.eq( "recipient", recipientName ) );
		cr.add( Restrictions.eq( "isNew", true ) );
		try {
			msgs = cr.list();
			count = msgs.size();
		} catch ( HibernateException he ) {
			LOGGER.error(he);
		}
		return count;
	}


	@SuppressWarnings("unchecked")
	public List<Message> readSentMessages( String senderName ) {
		LOGGER.debug(" Entering readSentMessages()" );
		List<Message> msgs = null;
		Criteria cr = getSession().createCriteria( Message.class );
		cr.add( Restrictions.eq( "sender", senderName ) ); // must match against the Class field name
		try {
			msgs = cr.list();
			if( msgs.isEmpty() ) {
				return msgs;
			}
		} catch ( HibernateException he ) {
			LOGGER.error(he);
		}

		return msgs;
	}
	
	public boolean updateMessageState( Message message ) {
		

		getSession().merge( message );
		
		return true;
	}
}
