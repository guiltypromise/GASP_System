/**
 * UserDAO.java
 * 
 * Provides the data layer to perform SCRUD operations to the database for
 * User type.
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.dao;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.tutorial.spring.model.User;


@Repository("userDAO")
public class UserDAO {

	private static final Logger LOGGER = Logger.getLogger(UserDAO.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Grabs the session from Hibernate to perform persistence.
	 * @return
	 */
	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	
	/**
	 * Inserts a provided and valid User into the database
	 * @param user
	 * @return
	 */
	public boolean insertUser(User user) {
		LOGGER.info("Entering insertData()");
				
		User foundUser = this.searchUser( user.getUsername() );
		
		if ( foundUser != null ) {
			return false;
		}
		
		getSession().persist(user); // does an Insert Into Table query with Hibernate

		return true;
	}
	
	@SuppressWarnings("unchecked")
	public User searchUser(String username) {
		
		LOGGER.debug(" Entering searchUser()" );
		
		Criteria cr = getSession().createCriteria( User.class );
		
		cr.add( Restrictions.eq( "username", username ) ); // filter down the result set of Users
		
		List<User> resultSet = cr.list();	// Execute query
		
		if( !resultSet.isEmpty() ) { // if we have a resulting record retrieved with the
									 // specified username query
			
			User user = resultSet.get(0);	
			LOGGER.debug( "searchUser(): Retrieved <- " + user.getUsername() );
			return user;
		}
		else {
			LOGGER.debug(" inside searchUser(): no record with username " + username );
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public boolean searchUserEmail( String email ) {
		
		LOGGER.debug(" Entering searchUserEmail()" );
		
		boolean flag = false;
		
		Criteria cr = getSession().createCriteria( User.class );
		
		cr.add( Restrictions.eq( "email", email ) ); // filter down the result set of Users
		
		List<User> resultSet = cr.list();	// Execute query
		
		if( !resultSet.isEmpty() ) { // if we have a resulting record retrieved with the
			 // specified username query

			User user = resultSet.get(0);
			LOGGER.info("searchUserEmail(): Retrieved <- " + user.getEmail());
			flag = true;
		} else {
			LOGGER.info(" inside searchUserEmail(): no record with email => " + email);
		}
		
		return flag;
	}
	
	@SuppressWarnings("unchecked")
	public User searchUserWithPw(String username, String password) {
		LOGGER.debug(" Entering searchUserWithPw()" );

		Criteria cr = getSession().createCriteria( User.class );
		
		cr.add( Restrictions.eq( "username", username ) );
		
		List<User> resultSet = cr.list();		
		
		if( !resultSet.isEmpty() ) {

			User user = resultSet.get(0);	
			LOGGER.debug( "searchUser( username, pw) Retrieved: " + user.getUsername() );

			return user; // found the username record, pass its hashed pw up also
		}

		return null; // else no user found
	}
	
	@SuppressWarnings("unchecked")
	public List<User> listUsers () {
		LOGGER.debug(" Entering listUsers()" );

		List<User> list;
		
		Criteria cr = getSession().createCriteria( User.class );
		
		list = cr.list();
		
		return list;
	}


	public boolean updateUser(User user) {
		LOGGER.debug("DAO is preparing to UPDATE given user");
		
		// check if we even have an existing user to update
		if( this.searchUserByID(user.getUserID() ) == null ) {
			LOGGER.debug("Could not find user to UPDATE");
			return false;
		}
		
		getSession().merge(user);
		
		
		
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public User searchUserByID(int userID) {
		LOGGER.debug(" Entering searchUserByID()" );
		
		Criteria cr = getSession().createCriteria( User.class );
		
		cr.add( Restrictions.eq( "userID", userID ) ); // filter down the result set of Users
		
		
		List<User> resultSet = cr.list();	// Execute query
		
		if( !resultSet.isEmpty() ) { // if we have a resulting record retrieved with the
									 // specified username query
			
			User user = resultSet.get(0);	
			LOGGER.debug( "searchUser(): Retrieved <- " + user.getUsername() );
			return user;
		}
		else {
			LOGGER.debug(" inside searchUser(): no record with userID " + userID );
		}
		
		return null;
	}


	public boolean deleteUser(User user) {
		User userToDelete = this.searchUser(user.getUsername() );
		
		// check if we even have an existing user to delete
		if(  userToDelete == null ) {
			LOGGER.info("Could not find user to DELETE");
			return false;
		}

		userToDelete = (User) getSession().merge(userToDelete);
		LOGGER.info("DAO is preparing to DELETE given user: " + userToDelete.getUsername() );

		getSession().delete(userToDelete);

		return true;
	}
}
