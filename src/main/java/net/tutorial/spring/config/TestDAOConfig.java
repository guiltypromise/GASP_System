package net.tutorial.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.tutorial.spring.dao.MessageDAO;
import net.tutorial.spring.dao.UserDAO;

@Configuration
public class TestDAOConfig {
	
	@Bean
	public UserDAO userDAO() {
		return new UserDAO();
	}
	
	@Bean
	public MessageDAO messageDAO() {
		return new MessageDAO();
	}
	
}
