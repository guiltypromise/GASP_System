/**
 * HibernateConfig.java
 * 
 * Provide configuration settings for database and entity persistence.
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan( { "net.tutorial.spring" } )
@PropertySource( value = { "classpath:jdbc.properties" } )
public class HibernateConfig {
	
	@Autowired
	private Environment env; // encapsulates the properties from PropertySource for runtime
	
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource( dataSource() );
		sessionFactory.setPackagesToScan( "net.tutorial.spring.model" );
		sessionFactory.setHibernateProperties( hibernateProperties() );
		return sessionFactory;
	}
	

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		// set the data source using properties gathered by Environment
		dataSource.setDriverClassName( env.getRequiredProperty( "jdbc.driverClassName" ));
		dataSource.setUrl( env.getRequiredProperty( "jdbc.url" ));
		dataSource.setUsername( env.getRequiredProperty( "jdbc.username" ));
		dataSource.setPassword( env.getRequiredProperty( "jdbc.password" ));

		return dataSource;
	}
	
	private Properties hibernateProperties() {
		Properties props = new Properties();
		
		props.put( "hibernate.dialect", env.getRequiredProperty( "hibernate.dialect" ) );
		props.put( "hibernate.show_sql", env.getRequiredProperty( "hibernate.show_sql" ) );
		props.put( "hibernate.formate_sql", env.getRequiredProperty( "hibernate.format_sql" ) );
		
		return props;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager( SessionFactory s ) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory( s );
		return txManager;
	}
}
