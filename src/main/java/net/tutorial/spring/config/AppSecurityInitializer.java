/**
 *  AppSecurityInitializer.java
 *  
 *  In reference to tutorial: http://websystique.com/spring-security/spring-security-4-hello-world-annotation-xml-example/
 *  
 *  @author jason.r.martin
 */

package net.tutorial.spring.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class AppSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
