/**
 *  AppSecurityConfig.java
 *  
 *  In reference to tutorial: http://websystique.com/spring-security/spring-security-4-hello-world-annotation-xml-example/
 *  
 *  @author jason.r.martin
 */

package net.tutorial.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;



@Configuration
@EnableWebSecurity
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("customUserDetailsService")
	UserDetailsService userDetailsService;
	
	/**
	 * In memory authentication to serve as testing credentials until 
	 * JDBC, LDAP, or other authentication back end is in place.
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("testAdmin").password("test").roles("ADMIN");
		auth.inMemoryAuthentication().withUser("testUser").password("test").roles("USER");
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	/**
	 * Configures web-based security for specific http requests
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {

	    http.authorizeRequests()
	    	.antMatchers("/admin").access("hasRole('ADMIN')")
	    	.antMatchers("/dashboard.htm").access("hasRole('USER') or hasRole('ADMIN')")
	    	.antMatchers("/messages").access("hasRole('USER') or hasRole('ADMIN')")
	    	.antMatchers("/messageSpa").access("hasRole('USER') or hasRole('ADMIN')")
	    	.antMatchers("/searchUserPage").access("hasRole('USER') or hasRole('ADMIN')")
	    .and()
	    	.formLogin()
	    	.loginPage("/login")
	    	.failureUrl("/login?error").defaultSuccessUrl("/#!/dashboardSpa")
	    	.usernameParameter("inputUsername")
	    	.passwordParameter("inputPassword")
		.and()
			.logout().logoutSuccessUrl("/logout")
		.and()
			.csrf()
		.and()
			.exceptionHandling().accessDeniedPage("/AccessDenied");
	    
	}
	
    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint(){
        BasicAuthenticationEntryPoint entryPoint = 
          new BasicAuthenticationEntryPoint();
        entryPoint.setRealmName("admin realm");
        return entryPoint;
    }
	
    
    @Bean 
    public PasswordEncoder passwordEncoder() {
    	return new BCryptPasswordEncoder();
    }
}
