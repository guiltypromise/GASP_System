/**
 * Custom Exception class for overriding SQL and CLASSNOTFOUND exceptions
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.exception;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class GASPGenericException extends Exception {
	//LOGGER to handle custom exceptions
	private static final Logger LOGGER = Logger.getLogger(GASPGenericException.class);
	
	public GASPGenericException(String message, Throwable object){
		super(message,object);
		LOGGER.info("Exception Message is :"+message);
	}
}

