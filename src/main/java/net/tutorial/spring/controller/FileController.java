/**
 * SearchController.java
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import net.tutorial.spring.model.User;
import net.tutorial.spring.service.UserFacade;



@Controller
public class FileController {
	
	private static final Logger LOGGER = LogManager.getLogger(FileController.class);
	
	private static final String LOCAL_FILE_PATH = "c://Users/jason.r.martin/Pictures/GASP_ProfilePics/";
	
	
	@Autowired
	private UserFacade userServiceImpl;
	
	@Autowired
	ServletContext context;
	 
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity<String> uploadImage( @RequestParam("file") MultipartFile file, HttpServletRequest request) {
		
		// get the current user on session
		User user = userServiceImpl.searchUser(getPrincipal());
		String userProfilePic = user.getAvatarPath(); // username_imgName 
		
		if( userProfilePic != null && !userProfilePic.isEmpty() ) { // if they have existing picture
			File oldFile = new File(LOCAL_FILE_PATH + userProfilePic);
			if( oldFile.delete()) {
				LOGGER.info("Old avatar pic removed");
			}
		}
		
		LOGGER.info( "Start of upload");
		
		// if we have file to upload
		if( file == null ) {
			LOGGER.error("Multipart request file empty");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(" Multipart File empty");

		}
		else if ( !file.isEmpty()) {

				String fileName = user.getUsername() + "_" + file.getOriginalFilename();
				//construct the path into server for the file
				String path = LOCAL_FILE_PATH + fileName;
				
				LOGGER.info("Storing file at: " + path);
				user.setAvatarPath(fileName);
				
				// make space for the file
				File destFile = new File(path);
				
				// write the data into the filesystem
				try {
					file.transferTo(destFile);
				} catch (IllegalStateException | IOException e) {
					LOGGER.error(e);
				}
				
				// update user database
				boolean result = userServiceImpl.updateUser(user);
				
				if ( result ) {
					LOGGER.info("Successfully saved profile pic path");
				}
				else {
					LOGGER.error("Unable to save profile pic path");
				}
			
		}
		
		return new ResponseEntity<>("{\"result\":\"success\"}", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getProfilePic", method = RequestMethod.GET)
	public void getProfilePic(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("Retrieving profile picture");
		// get the current user on session
		User user = userServiceImpl.searchUser(getPrincipal());
		String userProfilePic = user.getAvatarPath(); // username_imgName 
		String json = new Gson().toJson(user);
		if( userProfilePic == null || userProfilePic.isEmpty() ) {
		    try {
				response.getWriter().write("Picture not found");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				LOGGER.info(e);
			}
		}
		else {
			try {
				response.getWriter().write(json);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
	}
	
	
	private String getPrincipal() {
		String userName;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			LOGGER.info( "principal is instanceof UserDetails: " + principal.toString());
			
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		LOGGER.info( "getPrincipal() return: " + userName);
		
		return userName;
	}
}
