/**
 * AppController.java
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.tutorial.spring.model.User;
import net.tutorial.spring.service.UserFacade;

@Controller
@RequestMapping("/")
public class AppController {
	
	private static final Logger LOGGER = LogManager.getLogger(AppController.class);
	
	@Autowired
	private UserFacade userServiceImpl;
	
	@RequestMapping( value = {"/", "/home"} , method = RequestMethod.GET )
	public ModelAndView homePage(ModelMap model) {
		
		LOGGER.info("Entering homePage()");
		
		ModelAndView mv = new ModelAndView();
		
		// check provided user credentials
		User user = userServiceImpl.searchUser(getPrincipal());
		if( user != null ) {
			mv.addObject("user", user );
		}
		
		mv.setViewName("home");
		
		return mv;
	}
	
	@RequestMapping( value = {"/angularApp"} , method = RequestMethod.GET )
	public String angularAppPage(HttpServletRequest request) {
		LOGGER.info("Entering angularAppPage()");
		
		return "angularApp/index";
	}
	
	
	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			LOGGER.info( "principal is instanceof UserDetails: " + principal.toString());
			
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		LOGGER.info( "getPrincipal() return: " + userName);
		
		return userName;
	}
}