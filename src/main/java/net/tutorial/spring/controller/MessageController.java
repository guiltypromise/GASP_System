/**
 * MessageController.java
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import net.tutorial.spring.model.Message;
import net.tutorial.spring.model.User;
import net.tutorial.spring.service.MessageFacade;
import net.tutorial.spring.service.UserFacade;

@EnableScheduling
@Controller
public class MessageController 
{
	private static final Logger LOGGER = LogManager.getLogger(MessageController.class);

	@Autowired
	private UserFacade userServiceImpl;
	
	@Autowired
	private MessageFacade msgServiceImpl;
	
	private SimpMessagingTemplate template;
	
    @Autowired
    public MessageController(SimpMessagingTemplate template) {
        this.template = template;
    }
    
    @Scheduled( fixedRate = 2000 )
    public void scheduleNotify() {
    	int count;
    	List<User> users = userServiceImpl.showListUsers();
    	
    	for( User user : users ) {
    		count = msgServiceImpl.countNewMessagesForRecipient( user.getUsername() );
        	template.convertAndSend("/topic/notify-" + user.getUsername(), count);
    	}
    }
	
	@MessageMapping("/message")
	public void notification( String recipient ) {
		LOGGER.info("Entering notification()");
    	LOGGER.info("Notification for: " + recipient );
    	int count = msgServiceImpl.countNewMessagesForRecipient( recipient );
    	LOGGER.info( recipient + " has " + count + " new msgs");
    	template.convertAndSend("/topic/notify-" + recipient, count);
	}
	
	@RequestMapping( value = "/messageSpa", method = RequestMethod.GET )
	public ModelAndView messageSpaPage( HttpServletRequest request,
									HttpServletResponse response ) {
		LOGGER.info("Entering messageSpaPage()");

		ModelAndView mv = new ModelAndView();
		
		User user = userServiceImpl.searchUser( getPrincipal() );

		mv.addObject("user", user );
		mv.setViewName("messageSpa");
		
		return mv;
	}
	
	@RequestMapping( value = "/countNewMsg")
	public void countNewMsgs( HttpServletRequest request, HttpServletResponse response ) throws IOException {
		LOGGER.info("Entering countNewMsgs()");

		String recipient = request.getParameter("user");
		LOGGER.info("Counting new msgs for: " + recipient );
    	Integer count = msgServiceImpl.countNewMessagesForRecipient( recipient );
    	
    	response.getWriter().write( count.toString() );

	}
	
	@RequestMapping( value = "/sendMessage", method = RequestMethod.POST )
	public void sendMessage( HttpServletRequest request, 
							HttpServletResponse response ) throws IOException {
		LOGGER.info("Entering sendMessage()");
		
		Message messageObj = new Message();
		String sender = request.getParameter("sender");
		String recipient = request.getParameter("recipient");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");

		messageObj.setSenderName(sender);
		messageObj.setReceiverName(recipient);
		messageObj.setSubject(subject);
		messageObj.setMessage(message);
		
		LOGGER.info("Sender: " + request.getParameter("sender"));
		LOGGER.info("Recipient: " + messageObj.getReceiverName());
		LOGGER.info("Subject: " + messageObj.getSubject());
		LOGGER.info("Message: " + messageObj.getMessage());
		
		// quick validation if we have an existing recipient
		User recipientUser = userServiceImpl.searchUser( recipient );
		if( recipientUser == null ) {
			response.getWriter().write("fail");
		}
		else { // lets send the message
			boolean flag = msgServiceImpl.sendMessage(messageObj);
			if( flag ) {
				response.getWriter().write("success");	
			} else {
				response.getWriter().write("fail");
			}
		}
	}
	
	@RequestMapping( value = "/readSentMessages", method = RequestMethod.GET)
	public void getSentMessages( HttpServletRequest request, 
							HttpServletResponse response ) {
		LOGGER.debug("Entering getSentMessages()");
		
		User user = userServiceImpl.searchUser( getPrincipal() );
		String json;

		List<Message> listSentMsgs = msgServiceImpl.readSentMessages(user.getUsername());

		json = new Gson().toJson(listSentMsgs);
		

		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error(e); // refactor to send message back to client side ie. HTTP error, reason with ResponseEntity<>
		}
	}
	
	@RequestMapping( value = "/readReceivedMessages", method = RequestMethod.GET)
	public void getRecievedMessages( HttpServletRequest request, 
							HttpServletResponse response ) {
		LOGGER.debug("Entering getReceivedMessages()");
		User user = userServiceImpl.searchUser( getPrincipal() );

		List<Message> listRecvdMsgs = msgServiceImpl.readReceivedMessages(user.getUsername());
		
		
		String json = new Gson().toJson(listRecvdMsgs);
		
		

		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	
	@RequestMapping( value = "/readMessage", method = RequestMethod.GET )
	public void readMessage( HttpServletRequest request, 
							HttpServletResponse response ) throws IOException {
		LOGGER.info("Entering readMessage(");
		Message msg = new Message();
		String messageID = request.getParameter("messageID");
		msg.setMessageID( Integer.parseInt( messageID ) );
		msg.setNew( false );
		
		
		boolean flag = msgServiceImpl.updateMessage( msg );
		
		if( flag ) {
			response.getWriter().write("success");
		} else {
			response.getWriter().write("fail");
		}
		
	}
	
	@RequestMapping( value = "/getUserList.htm", method = RequestMethod.GET )
	public void getUserList( HttpServletRequest request,
							HttpServletResponse response ) {
		LOGGER.info("Entering getUserList()");
		
		List<User> listUsers = userServiceImpl.showListUsers();
		String json = new Gson().toJson(listUsers);

		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			LOGGER.debug( "principal is instanceof UserDetails: " + principal.toString());
			
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		LOGGER.debug( "getPrincipal() return: " + userName);
		
		return userName;
	}
}
