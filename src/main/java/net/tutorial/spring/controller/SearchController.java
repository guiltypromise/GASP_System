/**
 * SearchController.java
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import net.tutorial.spring.model.User;
import net.tutorial.spring.service.UserFacade;


@Controller
public class SearchController {
	
	private static final Logger logger = LogManager.getLogger(SearchController.class);
	
	@Autowired
	private UserFacade userServiceImpl;
	
	@RequestMapping( value = "/searchUserPage" , method = RequestMethod.GET )
	public ModelAndView performSearch( HttpServletRequest request ) throws IOException {
		ModelAndView mv = new ModelAndView();
		String statusMsg;
		String searchUsernameStr = request.getParameter("search");
		logger.info("Search Results: " + request.getParameter("search"));
		
		User searchUser = userServiceImpl.searchUser(searchUsernameStr);
		User user = userServiceImpl.searchUser( getPrincipal() );
		
		if( searchUser != null ) {
			mv.addObject("searchUserResult", searchUser);
			statusMsg = "success";
		}
		else {
			statusMsg = "fail";
		}
		
		mv.addObject( "user", user );
		mv.addObject( "status", statusMsg );
		mv.addObject( "resultString", searchUsernameStr );
		mv.setViewName("searchResult");
		return mv;
	}
	
	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			logger.info( "principal is instanceof UserDetails: " + principal.toString());
			
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		logger.info( "getPrincipal() return: " + userName);
		
		return userName;
	}
}
