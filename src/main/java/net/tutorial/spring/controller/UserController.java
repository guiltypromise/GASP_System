/**
 * UserController.java
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.controller;

import java.io.IOException;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;

import org.springframework.security.core.Authentication;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;

import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import org.	springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
	
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import net.tutorial.spring.exception.GASPGenericException;
import net.tutorial.spring.model.User;
import net.tutorial.spring.service.UserFacade;

@Controller
public class UserController {


	@Autowired
	private UserFacade userServiceImpl;
	

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);

	/**
	 * Mapping for a login page request.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView loginPage(HttpServletRequest request) {

		LOGGER.info("Entering loginPage()");

		ModelAndView mv = new ModelAndView();

		// check provided user credentials
		User user = userServiceImpl.searchUser(getPrincipal());
		if( user != null ) {
			mv.addObject("user", user );
		}
		mv.setViewName("login");

		// viewName, modelName, data
		return mv;
	}
	
	@RequestMapping(value = { "/#!/login" }, method = RequestMethod.POST)
	public ModelAndView loginPagePost(HttpServletRequest request) {

		LOGGER.info("Entering loginPagePost()");

		ModelAndView mv = new ModelAndView();

		// check provided user credentials
		User user = userServiceImpl.searchUser(getPrincipal());
		if( user != null ) {
			mv.addObject("user", user );
		}
		mv.setViewName("login");

		// viewName, modelName, data
		return mv;
	}

	/**
	 * Mapping for a logout page request.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public ModelAndView logoutPage(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("Entering logoutPage()");

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}

		ModelAndView mv = new ModelAndView();

		mv.setViewName("logout");
		return mv;
	}

	/**
	 * loginUser() Logs the user in using their provided username and password
	 * 
	 */
	@RequestMapping(value = "/dashboard.htm", method = RequestMethod.GET)
	public ModelAndView dashboardPage(HttpServletRequest request, HttpServletResponse response)
	        throws GASPGenericException {
		LOGGER.info("Entering dashboardPage()");

		User user = null;
		ModelAndView mv = new ModelAndView();

		// check provided user credentials
		user = userServiceImpl.searchUser( getPrincipal() );
		
		String json = new Gson().toJson(user);
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error( e );
		}

		if (user != null ) {
			mv.addObject("user", user);
			mv.addObject("json", json );
			mv.setViewName("dashboard");
		} else {
			mv.addObject("ERROR", "You need to be logged in to view that page");
			mv.setViewName("login");
		}
		return mv;
	}
	
	/**
	 * Mapping to the Single-Page-Application demo page
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping( value = "/spa", method = RequestMethod.GET )
	public ModelAndView spaPage( HttpServletRequest request,
									HttpServletResponse response ) {
		LOGGER.info("Entering spaPage()");

		ModelAndView mv = new ModelAndView();
		
		User user = userServiceImpl.searchUser( getPrincipal() );

		mv.addObject("user", user );
		mv.setViewName("spa");
		
		return mv;
	}

	/**
	 * Mapping to the register page.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = { "/register" }, method = RequestMethod.GET)
	public ModelAndView registerPage(ModelMap model) {
		LOGGER.info("Entering registerPage()");

		ModelAndView mv = new ModelAndView();
		
		User user = userServiceImpl.searchUser( getPrincipal() );
		
		if( user != null ) {
			mv.addObject( "ERROR", "You are already registered" );
			mv.setViewName( "login" );
		}
		else {
			mv.setViewName("register");

		}

		return mv;
	}

	/**
	 * Creates a new user with data from the registration form request.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws GASPGenericException
	 */
	@RequestMapping(value = "/registerForm.htm", method = RequestMethod.POST)
	public ModelAndView registerUser(HttpServletRequest request, HttpServletResponse response)
	        throws GASPGenericException {
		LOGGER.info("Entering registerUser()");

		if (request == null || response == null) {
			LOGGER.info("Request or Response failed for registerUser");
			throw new GASPGenericException("User Register Error", new NullPointerException());
		}

		User user = new User();
		ModelAndView mv = new ModelAndView();

		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		user.setFirstName(firstname);
		user.setLastName(lastname);
		user.setEmail(email);
		user.setUsername(username);
		user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt())); // encrypt
		                                                             // pw ASAP
		user.setEnabled(true);
		user.setRole("USER"); // default user type

		boolean insertStatus = userServiceImpl.createUser(user);

		LOGGER.info("registerUser(): Set user information for " + user.getUsername() + ":" + user.getPassword());
		
		if (insertStatus) {
			mv.addObject("REGISTERSTATUSMESSAGE", "You are now registered! You may now login.");
			mv.setViewName("registerConfirm");
		} else {
			mv.addObject("REGISTERSTATUSMESSAGE", "Registration failed, please try again.");
			mv.setViewName("registerConfirm");
		}

		// viewName, modelName, data
		return mv;
	}
	
	@RequestMapping( value = "/editProfile.htm", method = RequestMethod.POST )
	public void editProfile(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("Entering editProfile()");
		
		User user = userServiceImpl.searchUser( getPrincipal() );
		

		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		

		
		if( !username.isEmpty() ) {
			user.setUsername(username);
		}
		if( !firstname.isEmpty() ) {
			user.setFirstName(firstname);
		}
		if( !lastname.isEmpty() ) {
			user.setLastName(lastname);
		}
		if( !email.isEmpty() ) {
			user.setEmail(email);
		}
		
		boolean returnFlag = userServiceImpl.updateUser(user);
		if( returnFlag ) {
			user = userServiceImpl.searchUserByID( user.getUserID() );
			String json = new Gson().toJson(user);
			try {
				response.getWriter().write(json);
			} catch (IOException e) {
				LOGGER.error( e );
			}
		} else {
			LOGGER.error("Unable to modify/update User in database");
		}
	}
	
	@RequestMapping( value = "/profileConfirm" )
	public ModelAndView editProfileConfirm(HttpServletRequest request, HttpServletResponse response ) {
		LOGGER.info("Entering editProfileConfirm()");

		ModelAndView mv = new ModelAndView();
		User user = userServiceImpl.searchUser( getPrincipal() );
		
		mv.addObject("user", user);
		mv.addObject("STATUSMESSAGE", "Your profile has been updated." );
		mv.setViewName("profileConfirm");
		return mv;
	}
	
	@RequestMapping( value = "/checkUserPass.htm", method = RequestMethod.POST )
	public void checkUserPw(HttpServletRequest request, HttpServletResponse response) {
		User user = userServiceImpl.searchUser( getPrincipal() );
		String password = request.getParameter("password");
		
		User checkUser = userServiceImpl.searchUserWithPw(user.getUsername(), password);
		String status;
		if( checkUser != null ) {
			status = "valid";
		}
		else {
			status = "fail";
		}
		
		try {
			response.getWriter().print(status);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	
	@RequestMapping( value = "/changePassword.htm", method = RequestMethod.POST )
	public void changeUserPw(HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("Entering changePassword()");
		User user = userServiceImpl.searchUser( getPrincipal() );
		String currentPw = request.getParameter("oldPassword");
		String newPw = request.getParameter("newPassword");
		boolean checkPass = BCrypt.checkpw(currentPw, user.getPassword());
		
		if( checkPass ){
			user.setPassword(BCrypt.hashpw(newPw, BCrypt.gensalt()));
			if( userServiceImpl.updateUser(user) ) {
				user = userServiceImpl.searchUserByID( user.getUserID() );
				String json = new Gson().toJson(user);
				try {
					response.getWriter().write(json);
				} catch (IOException e) {
					LOGGER.error( e );
				}
			}
			else {
				LOGGER.error("Unable to update user pw");
			}
		}
	}
	
	@RequestMapping( value = "/getCurrentUser.htm", method = RequestMethod.GET )
	public void getCurrentUser(HttpServletRequest request, HttpServletResponse response ) {
		User user = userServiceImpl.searchUser( getPrincipal() );
		String json = new Gson().toJson(user);

		if( user == null) {
			json = "ERROR";
		}
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error( e );
		}
	}
	
	@RequestMapping( value = "/authenticate.htm", method = RequestMethod.GET )
	public ResponseEntity<String> authenticate(HttpServletRequest request, HttpServletResponse response ) {
		User user = userServiceImpl.searchUser( getPrincipal() );
		String json = new Gson().toJson(user);

		if( user == null ) {
			return new ResponseEntity<>("UN-AUTH", HttpStatus.FORBIDDEN);
		}
		
		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error( e );
		}
		
		return new ResponseEntity<>("AUTH", HttpStatus.OK);
	}

	/**
	 * Maps the access denied page
	 * 
	 * @return
	 */
	@RequestMapping(value = "/AccessDenied", method = RequestMethod.GET)
	public ModelAndView accessDenied() {
		LOGGER.info("Entering accessDenied()");

		ModelAndView model = new ModelAndView();
		User user;
		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			user = userServiceImpl.searchUser(getPrincipal());
			model.addObject("user", user);
		}

		model.setViewName("accessDenied");
		return model;

	}

	/**
	 * Searches for the user name of interest against the db and returns
	 * "success" if found, "fail" otherwise
	 * 
	 * @param request
	 * @param response
	 * @throws GASPGenericException
	 */
	@RequestMapping(value = "/searchUser.htm", method = RequestMethod.GET)
	public void searchUserName(HttpServletRequest request, HttpServletResponse response) throws GASPGenericException {
		LOGGER.info("Entering searchUserName()");

		if (request == null || response == null) {
			LOGGER.info("Request or Response failed for searchUser");
			throw new GASPGenericException("User Register Error", new NullPointerException());
		}

		User user = null;
		boolean value = false;
		String userName = null;

		LOGGER.info("userName = " + request.getParameter("userName"));

		userName = request.getParameter("userName");

		try {

			user = userServiceImpl.searchUser(userName);

			LOGGER.info("value is " + value);

			if (user != null) {
				response.getWriter().print("success");
			} else {
				response.getWriter().print("fail");
			}
		} catch (IOException e) {
			LOGGER.error("Invalid Username", e);
		}
	}

	/**
	 * Searches for the user email of interest against the db and returns
	 * "success" if found, "fail" otherwise
	 * 
	 * @param request
	 * @param response
	 * @throws GASPGenericException
	 */
	@RequestMapping(value = "/searchUserEmail.htm", method = RequestMethod.GET)
	public void searchUserEmail(HttpServletRequest request, HttpServletResponse response) throws GASPGenericException {

		LOGGER.info(("Entering searchUserEmail()"));

		if (request == null || response == null) {
			LOGGER.info("Request or Response failed for searchUser");
			throw new GASPGenericException("User Register Error", new NullPointerException());
		}

		boolean value = false;
		String userEmail = null;

		LOGGER.info("userEmail = " + request.getParameter("userEmail"));

		userEmail = request.getParameter("userEmail");

		try {
			value = userServiceImpl.searchUserEmail(userEmail);

			LOGGER.info("value is " + value);

			if (value) {
				response.getWriter().print("success");
			} else {
				response.getWriter().print("fail");
			}
		} catch (IOException e) {
			LOGGER.error("Invalid User Email", e);
		}
	}

	private String getPrincipal() {
		String userName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			LOGGER.info( "principal is instanceof UserDetails: " + principal.toString());
			
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		LOGGER.info( "getPrincipal() return: " + userName);
		
		return userName;
	}
}
