/**
 * AdminController.java
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import net.tutorial.spring.exception.GASPGenericException;
import net.tutorial.spring.model.User;
import net.tutorial.spring.service.UserFacade;

@Controller
public class AdminController {


	@Autowired
	private UserFacade userServiceImpl;
	

	private static final Logger LOGGER = LogManager.getLogger(AdminController.class);

	
	
	@RequestMapping( value = "/admin", method = RequestMethod.GET )
	public ModelAndView adminPage( HttpServletRequest request,
									HttpServletResponse response ) {
		LOGGER.info("Entering adminPage()");

		ModelAndView mv = new ModelAndView();
		
		User user = userServiceImpl.searchUser( getPrincipal() );

		mv.addObject("user", user );
		mv.setViewName("admin");
		
		return mv;
	}
	
	@RequestMapping( value = "/adminListUsers.htm", method = RequestMethod.GET )
	public void adminListUsers( HttpServletRequest request,
									HttpServletResponse response ) {
		LOGGER.info("Entering adminListUsers()");
		
		List<User> listUsers = userServiceImpl.showListUsers();
		String json = new Gson().toJson(listUsers);

		try {
			response.getWriter().write(json);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	
	@RequestMapping( value = "/toggleUserStatus", method = RequestMethod.GET )
	public void adminToggleUserStatus( HttpServletRequest request,
									HttpServletResponse response ) {
		LOGGER.info("Entering adminToggleUserStatus()");
		String json = null;

		try {
			String username = request.getParameter("userName");
			User user = userServiceImpl.searchUser(username);
			if( user.isEnabled() ) {
				user.setEnabled(false);
			} else {
				user.setEnabled(true);
			}
			boolean flag = userServiceImpl.updateUser(user);
			
			if( flag ) {
				json = new Gson().toJson(user);	
				try {
					response.getWriter().write(json);
				} catch (IOException e) {
					LOGGER.error(e);
				}
			} else {
				request.setAttribute("ERROR", "Unable to modify user");
			}
		}
		catch( Exception e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * Creates a new user with data from the registration form request.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws GASPGenericException
	 */
	@RequestMapping(value = "/addUser.htm", method = RequestMethod.POST)
	public void registerUser(HttpServletRequest request, HttpServletResponse response)
	        throws GASPGenericException {
		LOGGER.info("Entering addUser()");

		if (request == null || response == null) {
			LOGGER.info("Request or Response failed for addUser");
			throw new GASPGenericException("User Register Error", new NullPointerException());
		}

		User user = new User();
		String json;

		String firstname = request.getParameter("firstName");
		String lastname = request.getParameter("lastName");
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String role = request.getParameter("role");

		user.setFirstName(firstname);
		user.setLastName(lastname);
		user.setEmail(email);
		user.setUsername(username);
		user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt())); // encrypt
		                                                             // pw ASAP
		user.setEnabled(true);
		user.setRole(role); // default user type

		boolean insertStatus = userServiceImpl.createUser(user);		
		if (insertStatus) {
			json = new Gson().toJson(user);	
			try {
				response.getWriter().write(json);
				request.setAttribute("SUCCESS", "Account created");
			} catch (IOException e) {
				LOGGER.error(e);
				request.setAttribute("ERROR", "Unable to reload user account");
			}
		} else {
			request.setAttribute("ERROR", "Unable to create user account");
		}

	}
	
	@RequestMapping( value = "/deleteUserAccount", method = RequestMethod.GET )
	public void adminDeleteUser( HttpServletRequest request,
								HttpServletResponse response ) {	
		LOGGER.info("Entering adminDeleteUser()");
		
		try {
			String username = request.getParameter("userName");
			User user = userServiceImpl.searchUser(username);
			boolean flag = userServiceImpl.deleteUser(user);
			
			if( flag ) {
				request.setAttribute("SUCCESS", "User has been removed from the system.");
			}
			else {
				request.setAttribute("ERROR", "Unable to modify user");
			}
		}
		catch( Exception e) {
			LOGGER.error(e);
			request.setAttribute("ERROR", "Connection to database lost");
		}
		
	}

	
	private String getPrincipal() {
		String userName;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			LOGGER.info( "principal is instanceof UserDetails: " + principal.toString());
			
			userName = ((UserDetails) principal).getUsername();
		} else {
			userName = principal.toString();
		}
		
		LOGGER.info( "getPrincipal() return: " + userName);
		
		return userName;
	}
}
