/**
 * UserServiceImpl.java
 * 
 * Provides the business layer for User type database operations
 * and performs necessary logic ie validation before calling UserDAO operations.
 * 
 * @author jason.r.martin
 */
package net.tutorial.spring.service;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tutorial.spring.dao.UserDAO;
import net.tutorial.spring.exception.GASPGenericException;
import net.tutorial.spring.model.User;

@Transactional
@Service("userService") // specialized Component
public class UserServiceImpl implements UserFacade {

	@Autowired
	private UserDAO userDAO;
	
	private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
	
	@Override
	public boolean createUser(User user) {	
		LOGGER.info("Entering createUser()");
		
		// perform business layer validation
		
		// validate first name is not empty
		if( user.getFirstName().length() < 1 ) {
			return false;
		}

		//validate last name is not empty
		if( user.getLastName().length() < 1 ) {
			return false;
		}
		
		// email check? AJAX call for unique
		// username check would be unique since AJAX handled it
		// password check? front-end Angular validation
		
		LOGGER.info("createUser(): User is valid, passing to UserDAO.insertData()");

		return userDAO.insertUser(user);
	}

	@Override
	public User searchUser( String username ) {
		LOGGER.debug("Entering searchUser()");
		return  userDAO.searchUser(username);
	}
	
	@Override
	public User searchUserByID( int userID ) {
		LOGGER.info("Entering searchUser()");
		return  userDAO.searchUserByID(userID);
	}
	
	@Override
	public boolean searchUserEmail( String email ) {
		LOGGER.info("Entering searchUser()");
		return  userDAO.searchUserEmail(email);
	}

	@Override
	public User searchUserWithPw(String username, String password) {
		LOGGER.info("Entering searchUser() with user and pw");
		
		User user = userDAO.searchUserWithPw(username, password);
		
		// if we did retrieve a user,
		// Validate provided plain text pw with stored hashed version
		if( user != null && BCrypt.checkpw( password, user.getPassword() ) ) {
			return user; // return the user if provided passphrase matches 
		}
		
		return null; // invalid user credentials
	}
	
	@Override
	public List<User> showListUsers() {
		
		List<User> list = userDAO.listUsers();
 		return list;
	}

	@Override
	public boolean updateUser(User user) {
		LOGGER.info(" Calling DAO to update provided user" );

		return userDAO.updateUser( user );
	}
	
	@Override
	public boolean deleteUser(User user) {
		LOGGER.info(" Calling DAO to delete provided user" );

		return userDAO.deleteUser( user );
	}
}
