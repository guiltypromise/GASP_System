package net.tutorial.spring.service;

import java.util.List;

import net.tutorial.spring.exception.GASPGenericException;
import net.tutorial.spring.model.User;

public interface UserFacade {
	
	public boolean createUser( User user );
	
	public User searchUser( String username );
	
	public User searchUserByID( int userID );
	
	public boolean searchUserEmail(String email);
	
	public User searchUserWithPw( String username, String password );
	
	public boolean updateUser(User user);
	
	public List<User> showListUsers();

	boolean deleteUser(User user);
}
