package net.tutorial.spring.service;

import java.util.List;

import net.tutorial.spring.exception.GASPGenericException;
import net.tutorial.spring.model.Message;
import net.tutorial.spring.model.User;

public interface MessageFacade {
	
	public boolean sendMessage(Message message);
	
	public Message getMessageById( Message message );
	
	public List<Message> readReceivedMessages( String recipientName );
	
	public List<Message> readSentMessages( String senderName );
	
	public int countNewMessagesForRecipient( String recipientName );
	
	// modify/update
	public boolean updateMessage( Message message );
	
	public boolean deleteMessage();
}
