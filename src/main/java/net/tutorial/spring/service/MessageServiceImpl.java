/**
 * MessageServiceImpl.java
 * 
 * @author jason.r.martin
 */

package net.tutorial.spring.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tutorial.spring.dao.MessageDAO;
import net.tutorial.spring.dao.UserDAO;
import net.tutorial.spring.model.Message;

@Transactional
@Service("messageService")
public class MessageServiceImpl implements MessageFacade {
	
	@Autowired
	private MessageDAO messageDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	private static final Logger LOGGER = Logger.getLogger(MessageServiceImpl.class);

	@Override
	public boolean sendMessage( Message message ) {
		LOGGER.info("Entering send/createMessage()");
		
		return messageDAO.createMessage(message);
	}
	
	@Override
	public Message getMessageById(Message message) {
		return messageDAO.getMessageById(message);
	}

	@Override
	public List<Message> readReceivedMessages( String recipientName ) {
		LOGGER.debug("Entering readReceivedMessages()");

		return messageDAO.readReceivedMessages( recipientName );
	}
	
	@Override
	public List<Message> readSentMessages( String senderName ) {
		LOGGER.debug("Entering readSentMessages()");

		return messageDAO.readSentMessages( senderName );
	}
	
	@Override
	public int countNewMessagesForRecipient( String recipient ) {
		return messageDAO.countNewMessages( recipient );
	}

	@Override
	public boolean updateMessage( Message message ) {
		Message oldMsg = messageDAO.getMessageById(message);
		oldMsg.setNew(message.isNew());
		return messageDAO.updateMessageState(oldMsg);
	}
	
	@Override
	public boolean deleteMessage() {
		LOGGER.info("Entering deleteMessage");

		return false;
	}


	
}
