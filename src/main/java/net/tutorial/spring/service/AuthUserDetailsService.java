package net.tutorial.spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.tutorial.spring.controller.UserController;
import net.tutorial.spring.dao.UserDAO;
import net.tutorial.spring.model.UserRole;

@Service("customUserDetailsService")
public class AuthUserDetailsService implements UserDetailsService {
	
	private static final Logger LOGGER = LogManager.getLogger(AuthUserDetailsService.class);
	
	private static final String ROLE_PREFIX = "ROLE_"; // Spring Security needs this prefixed to roles
	
	@Autowired
	private UserDAO userDAO;

	@Transactional( readOnly = true )
	@Override
	public UserDetails loadUserByUsername( final String username ) throws UsernameNotFoundException {
		LOGGER.info( "Entering loadUserByUsername(): " + username );
		net.tutorial.spring.model.User user = userDAO.searchUser( username );
		
		if(user==null){
            throw new UsernameNotFoundException("Username not found");
		}
		LOGGER.info( "Found: " + user.getUsername() );
		Set<UserRole> userRole = new HashSet<>();
		UserRole roleObj = new UserRole();
		roleObj.setRole( user.getRole() );
		userRole.add( roleObj );
		List<GrantedAuthority> authorities = buildUserAuths( user.getRole() );
		return buildUserForAuth( user, authorities );
	}
	
	/**
	 * Convert app User into Spring Security User
	 * @param user
	 * @param auths
	 * @return
	 */
	private User buildUserForAuth( net.tutorial.spring.model.User user, List<GrantedAuthority> auths ) {
		LOGGER.info( "Entering buildUserForAuth() ");

		return new User( user.getUsername(), user.getPassword(), user.isEnabled(), 
						true, true, true, auths );
	}

	private List<GrantedAuthority> buildUserAuths( String role ) {
		LOGGER.info( "Entering buildUserAuths() ");

		
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// add user's authorities

		setAuths.add(new SimpleGrantedAuthority( ROLE_PREFIX + role));

		return new ArrayList<GrantedAuthority>(setAuths);
	}
}
