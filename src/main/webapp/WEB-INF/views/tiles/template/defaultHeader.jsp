<!-- 
	defaultHeader.jsp
	

	@author jason.r.martin
 -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!-- Static navbar -->
<nav class="navbar navbar-default navbar-fixed-top" >
	<div class="container-fluid">
		<div class="navbar-header">
			<button id="header_Button_ToggleNav" type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a id="header_Link_Home" class="navbar-brand" href="${pageContext.request.contextPath}/"><spring:message code="header.brand"/></a>
			<c:if test="${user != null }" > 
				<a class="navbar-brand" id="messageNotify" href="${pageContext.request.contextPath}/messageSpa"> 
					<span class="glyphicon glyphicon-envelope"></span> 
				</a>
		
					<span class="badge badge-error" data-ng-cloak data-ng-show="msgBadge > 0 "> {{ msgBadge }}</span>
					<span class="badge" data-ng-cloak data-ng-show="msgBadge == 0 "> {{ msgBadge }}</span>
				

			</c:if>
		</div>


		<div class="navbar-collapse collapse" id="navbar">
			<ul class="nav navbar-nav navbar-right">
 				<c:if test="${user != null }" > 
				<li>
					<form data-ng-cloak class="navbar-form" role="search" data-ng-controller="createMsgController" action="searchUserPage">
						<div class="input-group add-on">
							<input data-ng-model="vm.recipient" class="form-control" placeholder='<spring:message code="header.nav.search.placeholder"/>' name="search"
								id="search" type="text" autocomplete="off" data-ng-change="search(vm.recipient)">
							<div id="userHeaderResultsList" class="search-group list-group"
								data-ng-show="vm.showUserList">
								<a id="userHeaderResultsListItem" class="list-group-item"
									data-ng-repeat="recipient in results" data-ng-model="searchValue"
									data-ng-click="selectUser(recipient)">
									{{recipient.firstName }} {{ recipient.lastName }}
									({{recipient.username}})</a>
							</div>
								
							<div class="input-group-btn">
								<button id="header_Button_Submit" class="btn btn-primary" type="submit" data-ng-click="searchQuery(recipient)">
									<i class="glyphicon glyphicon-search"></i> <spring:message code="header.nav.search.button.submit"/>
								</button>
							</div>
						</div>
					</form>
				</li>
				
				</c:if> 
				
				<li class="active">
					<c:if test="${user != null }" > 
						<li class="dropdown">
							<a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false"> 
								<img id="headerProfilePic" data-ng-src='<spring:message code="profilePicDir2"/>${user.avatarPath}'/>
								<c:out value="${user.username }" /> <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<li><a href="${pageContext.request.contextPath}/"><span class="glyphicon glyphicon-home"></span> <spring:message code="header.nav.home"/></a></li>
								<li><a href="${pageContext.request.contextPath}/dashboard.htm"><span class="glyphicon glyphicon-th-list"></span> <spring:message code="header.nav.dashboard"/></a></li>
								<li><a href="${pageContext.request.contextPath}/messageSpa"> <span class="glyphicon glyphicon-envelope"></span> <spring:message code="header.nav.messages"/></a></li>
								<li><a href="${pageContext.request.contextPath}/spa"> <span class="glyphicon glyphicon-wrench"></span> <spring:message code="header.nav.spa"/></a></li>
								<c:if test="${user.role == 'ADMIN' }" > 
									<li><a href="${pageContext.request.contextPath}/admin"><span class="glyphicon glyphicon-user"></span> <spring:message code="header.nav.admin"/></a></li>
								</c:if>
								<li role="separator" class="divider"></li>
								<li>
									<a href="${pageContext.request.contextPath}/logout">
									<span class="glyphicon glyphicon-log-out"></span> 
									<spring:message code="header.nav.logout"/></a>
								</li>
							</ul>
						</li>
					</c:if>
					
					<c:if test="${user == null }" > 
						<a href="${pageContext.request.contextPath}/login">
							<span class="glyphicon glyphicon-log-in"></span> 
							<spring:message code="header.nav.login"/></a>
					</c:if>
				</li>
				
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<input type="hidden" name="sender" id="sender" ng-model="sender" value="${user.username}" />

