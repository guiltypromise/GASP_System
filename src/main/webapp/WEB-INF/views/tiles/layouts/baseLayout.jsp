<!-- 
	baseLayout.jsp
	
	@author jason.r.martin
 -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="import" tagdir="/WEB-INF/tags/imports" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/headers" %>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="_csrf" content="${_csrf.token}"/>
		<meta name="_csrf_header" content="${_csrf.headerName}"/>
		<meta name="viewport" content="width=device-width">
		<title><tiles:getAsString name="title" /></title>
		
		<%-- Static Files import --%>
		<import:staticFiles/>
		
		<!-- Custom styles import -->
		<import:cssFiles/>
	</head>
	
	<body data-ng-app="myApp">
		<div>
			<div data-ng-controller="headerController as vm">
				<header id="header">
					<header:headerSection/>
				</header>
			</div>
			
			<section id="site-content" data-ng-cloak>
			    <tiles:insertAttribute name="body" />
				<!-- <div data-ui-view></div> -->
			</section>
	
			<footer id="footer">
				<tiles:insertAttribute name="footer" />
			</footer>
	
			<input type="hidden" name="sender" id="sender" data-ng-model="sender"
				value="${user.username}" />
		</div>
	
	</body>
</html>