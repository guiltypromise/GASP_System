<!-- 
	logout.jsp
	
	@author jason.r.martin
 -->

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<head>
	<!--  Custom CSS for dashboard Page -->
		<link href="<c:url value='static/css/logout.css'/>" rel="stylesheet">

</head>

<div class="site-wrapper">
	<div class="site-wrapper-inner">
		<div class="cover-container">
			<div class="inner cover">

				<h1 class="cover-heading"><spring:message code="logout.confirmation"/></h1>
			</div>
		</div>
	</div>
</div>
