<!-- 
	messages.jsp
	
	@author jason.r.martin
 -->
<%@page session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<head>
<!--  Custom CSS for dashboard Page -->
<link href="<c:url value='static/css/messages.css'/>" rel="stylesheet">

</head>

<div class="site-wrapper">

	<h1 class="cover-heading"><spring:message code='messagePage.title'/></h1>


<div ng-cloak  ng-controller="messageController">
	<div class="container">
		<div class="site-inner">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-info">
						<div class="panel-heading"><spring:message code='messagePage.options.header'/></div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li class="active"><a data-toggle="tab"
									href="#createMessage"> <span
										class="glyphicon glyphicon-pencil"></span> <spring:message code='messagePage.options.new'/>
								</a></li>
								<li><a data-toggle="tab" href="#inbox"> <span
										class="glyphicon glyphicon-inbox"></span> <spring:message code='messagePage.options.inbox'/> <span class="label label-success" ng-show="inboxCount > 0">{{ inboxCount }} Unread</span></h4>
								</a></li>
								<li><a data-toggle="tab" href="#sent"> <span
										class="glyphicon glyphicon-send"></span> <spring:message code='messagePage.options.sent'/>
								</a></li>
							</ul>
						</div>
					</div>
				</div>
				

					<div class="col-sm-8">
						<div class="tab-content">
							<div id="createMessage" class="tab-pane in active">
								<div class="panel panel-default">
									<div class="panel-heading"><spring:message code='messagePage.create.header'/></div>
									<div class="panel-body">
										<div class="row">
											<form id="messageForm" name="messageForm"
												class="form-horizontal" role="form" method="post">
												<div class="form-group">

													<label class="sender-label col-md-2" for="recipient"><spring:message code='messagePage.create.sendTo'/></label>
													<div class="col-md-9">
														<input type="text" class="form-control" id="recipient"
															name="recipient"
															placeholder="<spring:message code='messagePage.create.sendTo.placeholder'/>"
															autocomplete="off" ng-model="recipient"
															ng-change="search(recipient)" required>

														<div id="userResultsList" class="list-group" ng-show="showUserList == true">
															<a id="userResultsListItem" class="list-group-item"
																ng-repeat="recipient in results" ng-model="searchValue"
																ng-click="selectUser(recipient.username)">
																{{recipient.firstName }} {{ recipient.lastName }}
																({{recipient.username}})</a>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="subject-label col-md-2" for="subject"><spring:message code='messagePage.create.subject'/></label>
													<div class="col-md-9">
														<input type="text" class="form-control"
															placeholder="<spring:message code='messagePage.create.subject.placeholder'/>" autocomplete="off"
															name="subject" id="subject" ng-model="subject" required>
													</div>
												</div>
												<div class="form-group">
													<label class="message-label col-md-2" for="message"><spring:message code='messagePage.create.body'/></label>
													<div class="col-md-9">
														<textarea class="form-control" rows="5"
															placeholder="<spring:message code='messagePage.create.body.placeholder'/>" autocomplete="off"
															name="message" id="message" required ng-model="message"></textarea>
													</div>
												</div>
												<input type="hidden" id="csrfToken" value="${_csrf.token}" />
												<input type="hidden" id="csrfHeader"
													value="${_csrf.headerName}" /> <input type="hidden"
													name="sender" id="sender" ng-model="sender"
													value="${user.username}" />
												<div class="message-send-button">
													<button class="btn btn-primary"
														ng-disabled="messageForm.$invalid"
														ng-click="sendMsg( csrftoken ); messageForm.$setPristine()"
														data-toggle="modal" data-target="#myModal">Send</button>
												</div>
											</form>

										</div>

									</div>
								</div>
							</div>
							<div id="inbox" class="tab-pane">
								<div class="panel panel-default">				
									<div class="panel-heading"><spring:message code="messagePage.inbox.header"/></div>
									<div class="panel-body">		
									
										<div class="list-group">
											<a href="#" id="inboxItem" class="list-group-item" ng-repeat="msg in listRecvdMsgs | orderBy : '-messageID'" 
												ng-click="readMessage( msg.messageID )">
												
												<h4 class="list-group-item-heading"><spring:message code="messagePage.inbox.from"/> {{ msg.sender }} <span class="label label-success" ng-show="msg.isNew == true">New!</span></h4>
												<p class="list-group-item-text"><spring:message code="messagePage.inbox.subject"/> {{ msg.subject }}</p>
												<p class="list-group-item-text"><spring:message code="messagePage.inbox.body"/> {{ msg.message }}</p>
											</a> 
										</div>	
									</div>
								</div>

							</div>
							<div id="sent" class="tab-pane">
								<div class="panel panel-default">
									<div class="panel-heading"><spring:message code="messagePage.sent.header"/></div>
									<div class="panel-body">
										<div class="list-group">
											<a href="#" id="inboxItem" class="list-group-item" ng-repeat="msg in listSentMsgs | orderBy : '-messageID'">
												<h4 class="list-group-item-heading"><spring:message code="messagePage.sent.recipient"/> {{ msg.recipient }} <span class="label label-primary" ng-show="!msg.isNew">Received</span><span class="label label-default" ng-show="msg.isNew">Unopened</span></h4>
												<p class="list-group-item-text"><spring:message code="messagePage.sent.subject"/> {{ msg.subject }}</p>
												<p class="list-group-item-text"><spring:message code="messagePage.sent.body"/> {{ msg.message }}</p>
											</a> 
										</div>		
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><spring:message code="messagePage.confirm.header"/></h4>
        </div>
        <div class="modal-body">
          <div ng-show="sentSuccess" class="alert alert-success">
 			 <strong><spring:message code="messagePage.confirm.success"/></strong> 
		</div>
          <div ng-show="!sentSuccess" class="alert alert-danger">
 			 <strong><spring:message code="messagePage.confirm.error"/></strong> 
 			 <hr>
 			 <p><spring:message code="messagePage.confirm.error.info"/> '{{recipient}}'</p>
		</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="messagePage.confirm.button.close"/></button>
        </div>
      </div>
    </div>
  </div>
</div>

<!--  JS message controller  -->
<script src="static/js/controllers/messageController.js"></script>
<script src="static/js/filters/searchUserFilter.js"></script>
<script src="static/js/services/messageService.js"></script>

