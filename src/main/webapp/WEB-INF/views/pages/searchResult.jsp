<%--
	searchResult.jsp
	
	@author jason.r.martin
 --%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="site-wrapper">
	<div class="container">
		<div class="site-inner">

			<div class="well">
				<c:choose>
					<c:when test="${ status == 'success'  }">
						<h2>Search result for: '${resultString}'</h2>
					</c:when>
					<c:otherwise>
						<h2>No result found</h2>
					</c:otherwise>
				
				</c:choose>
			</div>
			<c:choose>
				<c:when test="${ status == 'success' }">
					<div class="panel panel-primary">
						<div class="panel-heading">
							${searchUserResult.firstName} ${ searchUserResult.lastName }
						</div>
						<div class="panel-body">
							<div class="col-md-6">
								<div id="profilePictureContainer" class="row">
									<img id="profilePicture" data-ng-src='<spring:message code="profilePicDir2"/>${searchUserResult.avatarPath}'/>
								</div>
								<table class="table-bordered">
									<tr>
										<td class="col-md-3">Username:  </td>
										<td colspan="2">${searchUserResult.username }</td>
									</tr>
									<tr>
										<td class="col-md-3">First Name:  </td>
										<td>${searchUserResult.firstName }</td>
									</tr>
									<tr>
										<td class="col-md-3">Last Name:  </td>
										<td>${searchUserResult.lastName }</td>
									</tr>
									<tr>
										<td class="col-md-3">Email:  </td>
										<td>${searchUserResult.email }</td>
									</tr>								
								</table>
							</div>
							<div class="col-md-6">
								<div class="panel panel-success">
									<div class="panel-heading">
									Info
									</div>
									<div class="panel-body">
										Bio info										
									</div>
								</div>
							</div>
						</div>
					</div>

				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>

