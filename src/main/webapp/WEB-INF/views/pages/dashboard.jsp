<!-- 
	dashboard.jsp
	
	@author jason.r.martin
 -->
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div data-ng-cloak data-ng-controller="editProfileController as pvm" >
<div class="site-wrapper" data-ng-controller="fileController as vm">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="cover-heading"><spring:message code="dashboard.title"/></h3>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-4">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 id="profileHeader">
								<spring:message code="dashboard.profile.header"/>
							</h3>
						</div>
						<div class="panel-body">
							<div id="profilePictureContainer" data-ng-cloak class="row">
								<p data-ng-show="!vm.imageSource"> No Profile Picture</p>
							 <img id="loadingPicture" data-ng-show="vm.isLoading" src="static/img/ajax-loader.gif"/>
								<p data-ng-show="vm.isLoading">Loading...</p>
								<img id="profilePicture" data-ng-show="!vm.isLoading" data-ng-cloak data-ng-src='<spring:message code="profilePicDir2"/>{{vm.imageSource}}'/>
							</div> 
							<table class="table table-sm table-bordered">
								<tbody>
									<tr>
										<th scope="row"><spring:message code="dashboard.profile.user"/> </th>
										<td id="view-userName">{{pvm.user.username}}</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code="dashboard.profile.firstName"/> </th>
										<td id="view-firstName"> {{pvm.user.firstName}} </td>
									</tr>
									<tr>
										<th scope="row"><spring:message code="dashboard.profile.lastName"/></th>
										<td id="view-lastName" >{{pvm.user.lastName }}</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code="dashboard.profile.email"/></th>
										<td id="view-email">{{pvm.user.email}}</td>
									</tr>
									<tr>
										<th scope="row"><spring:message code="dashboard.profile.accountType"/></th>
										<td>{{pvm.user.role}}</td>
									</tr>
								</tbody>
							</table>
							
							<div class="profile-button-group">
								<a class="btn btn-primary" data-toggle="modal"
									data-target="#editProfileModal" data-ng-click="status = false"> <span
									class="glyphicon glyphicon-edit"></span> <spring:message code="dashboard.profile.button.edit"/>
								</a>
								<button class="btn btn-primary" data-ng-click="pvm.openChangePwForm()">
									<em class="fa fa-key"></em> Change Password
								</button>
							</div>
							<div id="statusBox">
								<div id=statusConfirm>
									<div id="edit-status-confirm" data-ng-show="pvm.status || pvm.pwStatus" 
										data-ng-class="pvm.alertClass">
										<span data-ng-show="pvm.changesSaved">
											<spring:message code="dashboard.profile.status.confirm"/>
										</span>
										<span data-ng-show="pvm.passwordSaved">
											Password saved
										</span>
										<span data-ng-show="pvm.passwordNotSaved">
											Old password does not match current password
										</span>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-sm-6">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3><spring:message code="dashboard.main.header"/></h3>
						</div>
						<div class="panel-body">

							<p><spring:message code="dashboard.main.info"/></p>
							

								<div class="row">
									<div class="col-sm-6">
										<form method="post" enctype="multipart/form-data"
												name="uploadPicForm">
											<div class="form-group">
												<label for="file1">Profile Picture</label> 
						
												<input id="dashboard_File_UploadPic" 
													required data-file-upload="vm.myFile" 
													type="file" 
													name="file"
													accept=".jpg,.png,.gif"
													>
											</div>
											<button id="dashboard_Button_SubmitProfilePic" 
													class="btn btn-primary"
													data-ng-click="vm.uploadFile()"> Upload Profile Picture 
											</button>
											<p data-ng-show="vm.isError" class="error-text"> Error uploading image </p>
											
										</form>
									</div>
									<div class="col-sm-6">
									
									
									</div>
								</div>

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="editProfileModal" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">


			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h2 class="modal-title"><spring:message code="dashboard.profile.edit.header"/></h2>

			</div>
					<form id="editProfileForm" name="editProfileForm"
						class="form-horizontal" method="post" action="editProfile.htm">
				<div class="modal-body">

						<fieldset>
						<span id="usernameChangeWarning"> <spring:message code="dashboard.profile.edit.warning"/></span>
						<br>

							<!-- Text input-->
							<div class="form-group"
								
								data-ng-class="{ 'has-success' : editProfileForm.username.$valid && !editProfileForm.username.$error.userNameExist
							, 'has-error' : editProfileForm.username.$error.userNameExist }">
								<label class="col-md-4 control-label" for="username"><spring:message code="dashboard.profile.user"/></label>

								<div class="col-md-5">

									<input id="username" name="username" type="text"
										placeholder="Ex: johndoe" class="form-control input-md"
										required data-user-name-exist
										data-ng-model-options="{ updateOn: 'blur' }"
										data-ng-model="pvm.username">

								</div>
								<p data-ng-cloak data-ng-show="editProfileForm.username.$error.userNameExist"
									class="help-block"><spring:message code="dashboard.profile.edit.username.error"/></p>
								<p  data-ng-cloak data-ng-show="!editProfileForm.username.$invalid && !editProfileForm.username.$error.userNameExist"
									class="help-block"><spring:message code="dashboard.profile.edit.username.available"/></p>
							</div>


							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-4 control-label" for="firstname"><spring:message code="dashboard.profile.firstName"/></label>
								<div class="col-md-5">
									<input id="firstname" name="firstname" type="text"
										placeholder="Ex: John" class="form-control input-md" required
										autofocus data-ng-model-options="{updateOn: 'submit'}" data-ng-model="pvm.user.firstName">
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<label class="col-md-4 control-label" for="lastname"><spring:message code="dashboard.profile.lastName"/></label>
								<div class="col-md-5">
									<input id="lastname" name="lastname" type="text"
										placeholder="Ex: Doe" class="form-control input-md"
										 data-ng-model-options="{updateOn: 'submit'}"
										data-ng-model="pvm.user.lastName">

								</div>
							</div>

							<!-- Email input-->
							<div class="form-group"
								data-ng-class="{ 'has-success' : editProfileForm.email.$valid && !editProfileForm.email.$error.userEmailExist
								, 'has-error' : editProfileForm.email.$error.userEmailExist }">
								<label class="col-md-4 control-label" for="email"><spring:message code="dashboard.profile.email"/></label>
								<div class="col-md-5">
									<input id="email" name="email" type="email"
										required
										placeholder="Ex: johndoe@outlook.com"
										class="form-control input-md" data-user-email-exist
										data-ng-model-options="{ updateOn: 'blur' }"
										data-ng-model="pvm.email">
								</div>
								<p data-ng-cloak data-ng-show="editProfileForm.email.$error.userEmailExist"
									class="help-block"><spring:message code="dashboard.profile.edit.email.error"/></p>
								<p data-ng-cloak data-ng-show="!editProfileForm.email.$invalid && !editProfileForm.email.$error.userEmailExist"
									class="help-block"><spring:message code="dashboard.profile.edit.email.available"/></p>

							</div>

						</fieldset>

						<input type="hidden" id="csrfToken" value="${_csrf.token}" />
				</div>


				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">
						<spring:message code="dashboard.profile.edit.button.cancel"/></button>
					<button type="submit" class="btn btn-success" data-dismiss="modal"
						data-ng-click="pvm.sendEdit()"
						data-ng-disabled="editProfileForm.username.$error.userNameExist || editProfileForm.email.$error.userEmailExist || editProfileForm.$untouched">
						<span class="glyphicon glyphicon-floppy-save"></span> <spring:message code="dashboard.profile.edit.button.save"/>
					</button>
				</div>
				</form>
		</div>
	</div>
</div>
</div>

