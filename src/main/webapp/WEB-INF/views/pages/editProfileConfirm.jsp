<!-- 
	registerSuccess.jsp
	
	@author jason.r.martin
 -->

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<!--  Custom CSS for Home Page -->
	<link href="<c:url value='static/css/home.css'/>" rel="stylesheet">
</head>

<div class="site-wrapper">
	<div class="site-wrapper-inner">
		<div class="cover-container">
			<div class="inner cover">

				<h1 class="cover-heading"><c:out value ="${STATUSMESSAGE}"/></h1>

			</div>
		</div>
	</div>
</div>

