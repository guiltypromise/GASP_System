<!-- 
	admin.jsp
	
	@author jason.r.martin
 -->
<%@page session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div class="site-wrapper">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="cover-heading"><spring:message code="admin.title"/></h3>
		</div>
		<div class="panel-body">
			<div class="table-container">
				<div data-ng-controller="adminController as vm">
					<div class="row">
						<div class="col-md-1">
							<spring:message code="admin.options.header"/>
						</div>
						<div class="col-md-3"> 
							<button id="admin_Button_AddUser" class="btn btn-primary" data-ng-click="vm.openAddUserConfirm()"> <spring:message code="admin.options.button.add"/></button>
						</div>
						<div class="col-md-2"></div>
						<div class="col-md-6">
							<form>
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon">
											<em class="fa fa-search"></em>
										</div>
										<input type="text" class="form-control"
											placeholder="Enter in filter queries" data-ng-model="vm.filterUser">
									</div>
								</div>
							</form>
						</div>

					</div>
					<table id="adminUserTable" class="table table-sm table-bordered table-condensed table-hover table-striped">
						<thead>
							<tr>
								<th scope="col">
									<a href="#" data-ng-click="vm.sortFirstName(); vm.sortReverse()">
										<spring:message code="admin.table.header.firstName"/>
										<em data-ng-show="!vm.bSortFirstName" class="fa fa-sort pull-right"></em>
										<em data-ng-show="vm.bSortFirstName && !vm.bSortReverse" class="fa fa-sort-desc pull-right"></em>
										<em data-ng-show="vm.bSortFirstName && vm.bSortReverse" class="fa fa-sort-asc pull-right"></em>
									</a>
								</th>
								<th scope="col">
									<a href="#" data-ng-click="vm.sortLastName(); vm.sortReverse()">
										<spring:message code="admin.table.header.lastName"/>
										<em data-ng-show="!vm.bSortLastName" class="fa fa-sort pull-right"></em>
										<em data-ng-show="vm.bSortLastName && !vm.bSortReverse" class="fa fa-sort-desc pull-right"></em>
										<em data-ng-show="vm.bSortLastName && vm.bSortReverse" class="fa fa-sort-asc pull-right"></em>
									</a>
								</th>
								<th scope="col">
									<a href="#" data-ng-click="vm.sortEmail(); vm.sortReverse()">
										<spring:message code="admin.table.header.email"/>
										<em data-ng-show="!vm.bSortEmail" class="fa fa-sort pull-right"></em>
										<em data-ng-show="vm.bSortEmail && !vm.bSortReverse" class="fa fa-sort-desc pull-right"></em>
										<em data-ng-show="vm.bSortEmail && vm.bSortReverse" class="fa fa-sort-asc pull-right"></em>
									</a>
								
								</th>
								<th scope="col">
									<a href="#" data-ng-click="vm.sortUsername(); vm.sortReverse()">
										<spring:message code="admin.table.header.user"/>
										<em data-ng-show="!vm.bSortUsername" class="fa fa-sort pull-right"></em>
										<em data-ng-show="vm.bSortUsername && !vm.bSortReverse" class="fa fa-sort-desc pull-right"></em>
										<em data-ng-show="vm.bSortUsername && vm.bSortReverse" class="fa fa-sort-asc pull-right"></em>
									</a>
								</th>
								<th scope="col">
									<a href="#" data-ng-click="vm.sortRole(); vm.sortReverse()">
										<spring:message code="admin.table.header.type"/>
										<em data-ng-show="!vm.bSortRole" class="fa fa-sort pull-right"></em>
										<em data-ng-show="vm.bSortRole && !vm.bSortReverse" class="fa fa-sort-desc pull-right"></em>
										<em data-ng-show="vm.bSortRole && vm.bSortReverse" class="fa fa-sort-asc pull-right"></em>
									</a>
								</th>
								<th scope="col">
									<a href="#" data-ng-click="vm.sortStatus(); vm.sortReverse()">
										<spring:message code="admin.table.header.status"/>
										<em data-ng-show="!vm.bSortStatus" class="fa fa-sort pull-right"></em>
										<em data-ng-show="vm.bSortStatus && !vm.bSortReverse" class="fa fa-sort-desc pull-right"></em>
										<em data-ng-show="vm.bSortStatus && vm.bSortReverse" class="fa fa-sort-asc pull-right"></em>
									</a>
								</th>
								<th scope="col"><spring:message code="admin.table.header.actions"/></th>
							</tr>
						</thead>
						<tbody>
							<tr data-ng-cloak data-ng-repeat="user in vm.listUsers | orderBy:vm.orderByField:vm.bSortReverse | filter:vm.filterUser">
								<td>{{ user.firstName }}</td>
								<td>{{ user.lastName }}</td>
								<td>{{ user.email }}</td>
								<td>{{ user.username }}</td>
								<td>{{ user.role }}</td>
								<td>
									<span class="label label-success" data-ng-show="user.enabled"><spring:message code="admin.table.label.active"/></span>
									<span class="label label-danger" data-ng-show="!user.enabled"><spring:message code="admin.table.label.inactive"/></span>
								</td>
								<td>
									<div id="actionButtonGroup" data-ng-show="user.username != '${user.username}'">
										<button id="admin_Button_Modify" class="btn btn-primary" data-ng-disabled="user.role == 'ADMIN'"
											data-ng-click="vm.openModifyUser(user)" ><spring:message code="admin.table.button.modify"/></button>
										<button id="admin_Button_Enable" class="btn btn-warning" data-ng-model="user.enabled" data-ng-show="user.enabled" data-ng-disabled="user.role == 'ADMIN'"
											data-ng-click="vm.toggleAccountStatus(user)" ><spring:message code="admin.table.button.disable"/></button>
										<button id="admin_Button_Disable" class="btn btn-success" data-ng-model="user.enabled" data-ng-show="!user.enabled" data-ng-disabled="user.role == 'ADMIN'"
											data-ng-click="vm.toggleAccountStatus(user)" > <spring:message code="admin.table.button.enable"/></button>
										<button id="admin_Button_Delete" class="btn btn-danger" data-ng-disabled="user.role == 'ADMIN'" data-ng-click="vm.openDeleteConfirm( user )"><spring:message code="admin.table.button.delete"/></button>
										
									</div>
									
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>







