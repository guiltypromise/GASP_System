<!-- 
	spa.jsp
	
	@author jason.r.martin
 -->

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="site-wrapper body-white">
	<div data-ng-controller="spaController" class="inner cover container-fluid">
		<div class="row">
			<div id="mySidenav" class="sidenav">
				<a href="javascript:void(0)" type="button" class="closebtn" onclick="closeNav()">&times;</a>
				<hr>
				<a href="#!/home"><spring:message code="spaPage.header.link.home"/></a> 
				<a href="#!/infoStub"><spring:message code="spaPage.header.link.demo"/></a>
				<a href="#!/userDir"><spring:message code="spaPage.header.link.info"/></a>
				
			</div>
			<nav class="navbar navbar-inverse navbar-fixed">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="#!/"><spring:message code="spaPage.header.brand"/></a>
						<ul class="nav navbar-nav">
							<li><a href="#!/home"><spring:message code="spaPage.header.link.home"/></a></li>
							<li><a href="#!/infoStub"><spring:message code="spaPage.header.link.demo"/></a></li>
							<li><a href="#!/userDir"><spring:message code="spaPage.header.link.info"/></a></li>
						</ul>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<li><button id="spa_Navbar_MenuButton"class="btn btn-primary" onclick="openNav()"><spring:message code="spaPage.header.link.menu"/></button></li>
					</ul>
				</div>
			</nav>
		</div>
		<br>
		<div data-ui-view id="main" data-spy="scroll" data-target="#nav-topics" data-offset="10"></div>
	</div>
</div>

<script>
/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>


