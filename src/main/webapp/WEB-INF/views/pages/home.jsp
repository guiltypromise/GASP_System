<!-- 
	home.jsp
	
	@author jason.r.martin
 -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page session="false"%>

<div class="site-wrapper">
	<div class="site-wrapper-inner">
		<div class="cover-container">
			<div class="inner cover">
				<div class="jumbotron">
					<h3 class="cover-heading">
						<spring:message code = "homePage.greeting"/>
					</h3>
					<a href="angularApp">Go to Angular App</a>
				</div>
			</div>
		</div>
	</div>
</div>

