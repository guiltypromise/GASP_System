<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<!-- Bootstrap Imports  -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
	integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
	crossorigin="anonymous">
	
<!--  jQuery for bootstrap Navbar -->
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>


<%-- AngularJS Framework import  --%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/1.0.3/angular-ui-router.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-animate.js"></script>
<script src="static/js/build/angular-translate.js"></script>
<script src="static/js/build/angular-translate-loader-partial.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/1.2.4/ui-bootstrap-tpls.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate-loader-url/2.15.2/angular-translate-loader-url.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.15.2/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.15.2/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate/2.15.2/angular-translate-storage-cookie/angular-translate-storage-cookie.js"></script>

<script src="https://cdn.rawgit.com/angular-translate/bower-angular-translate-handler-log/2.15.2/angular-translate-handler-log.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.min.js"></script>

<!--  JS App and feature modules  -->
<script src="static/js/modules/myApp.js"></script>
<script src="static/js/modules/adminModule.js"></script>
<script src="static/js/modules/dashboardModule.js"></script>
<script src="static/js/modules/spaModule.js"></script>
<script src="static/js/modules/messageModule.js"></script>
<script src="static/js/modules/registerModule.js"></script>
<script src="static/js/modules/loginModule.js"></script>
<script src="static/js/modules/calendarModule.js"></script>


<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
	
<!--  Font awesome CDN -->
<link 
  href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet"  type='text/css'>

<!--  SockJS and STOMP for web socket -->
<script src="static/js/sockjs/sockjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>

<!-- Bootstrap core JavaScript static -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="static/js/controllers/adminController.js"></script>
<script src="static/js/controllers/adminDeleteModalController.js"></script>
<script src="static/js/controllers/adminAddModalController.js"></script>
<script src="static/js/controllers/adminModifyModalController.js"></script>
<script src="static/js/controllers/registerController.js"></script>
<script src="static/js/controllers/loginController.js"></script>
<script src="static/js/controllers/headerController.js"></script>
<script src="static/js/controllers/editProfileController.js"></script>
<script src="static/js/controllers/indexSpaController.js"></script>
<script src="static/js/controllers/mainMsgController.js"></script>
<script src="static/js/controllers/createMsgController.js"></script>
<script src="static/js/controllers/inboxMsgController.js"></script>
<script src="static/js/controllers/sentMsgController.js"></script>
<script src="static/js/controllers/spaController.js"></script>
<script src="static/js/controllers/widgetsController.js"></script>
<script src="static/js/controllers/calendarController.js"></script>
<script src="static/js/controllers/userDirController.js"></script>
<script src="static/js/controllers/fileController.js"></script>
<script src="static/js/controllers/changePwModalController.js"></script>


<script src="static/js/directives/userNameDirective.js"></script>
<script src="static/js/directives/userEmailDirective.js"></script>
<script src="static/js/directives/userPasswordCompareDirective.js"></script>
<script src="static/js/directives/calendarDirective.js"></script>
<script src="static/js/directives/fileUploadDirective.js"></script>


<script src="static/js/services/adminService.js"></script>
<script src="static/js/services/messageService.js"></script>
<script src="static/js/services/searchService.js"></script>
<script src="static/js/services/userService.js"></script>
<script src="static/js/services/editProfileService.js"></script>
<script src="static/js/services/translateService.js"></script>
<script src="static/js/services/fileService.js"></script>


<script src="static/js/filters/searchUserFilter.js"></script>

<script src="static/js/bundled.js"></script>
