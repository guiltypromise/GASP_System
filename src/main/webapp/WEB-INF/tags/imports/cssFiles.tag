<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%--  =======  CSS =========== --%>

<%-- home.jsp, spa.jsp, admin.jsp --%>
<link href="<c:url value='static/css/home.css'/>" rel="stylesheet">

<%-- register.jsp --%>
<link href="<c:url value='static/css/register.css'/>" rel="stylesheet">

<%-- login.jsp --%>
<link href="static/css/login.css" rel="stylesheet">

<%-- defaultHeader.jsp --%>
<link href="static/css/header.css" rel="stylesheet">

<%-- baseLayout.jsp --%>
<link href="<c:url value='static/css/base.css'/>" rel="stylesheet">

<%--  dashboard.jsp --%>
<link href="<c:url value='static/css/dashboard.css'/>" rel="stylesheet">

<%-- messageSpa.jsp --%>
<link href="<c:url value='static/css/messages.css'/>" rel="stylesheet">

<%-- spa.jsp --%>
<link href="<c:url value='static/css/calendar.css'/>" rel="stylesheet">

<%-- searchResult.jsp --%>
<link href="<c:url value='static/css/searchResult.css'/>" rel="stylesheet">
