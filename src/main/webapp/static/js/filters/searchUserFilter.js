angular.module('myApp').filter('searchUserFilter', [ function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, searchString){

		// $log
		console.log("Search query: " + searchString.toLowerCase() );
		if(!searchString){
			return arr;
		}

		var result = [];

		// Using the forEach helper method to loop through the array

		angular.forEach(arr, function(user){
			searchLine1 = user.firstName + user.lastName + user.username;
			searchLine2 = user.lastName +  user.firstName + user.username;
			searchLine1 = searchLine1.toLowerCase().replace(/\s/g, '');
			searchLine2 = searchLine2.toLowerCase().replace(/\s/g, '');
			if ((searchLine1
					.indexOf(searchString.toLowerCase().trim().replace(/\s/g,'')) !== -1) || (searchLine2
							.indexOf(searchString.toLowerCase().trim().replace(/\s/g,'')) !== -1 ) ) {
				result.push(user);
			}
		});

		return result;
	};

}]);