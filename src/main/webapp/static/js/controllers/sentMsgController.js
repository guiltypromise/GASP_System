'use strict';
angular.module('messageModule').controller('sentMsgController',  ['$scope', '$translate', 'messageService', 
									function($scope, $translate, messageService) {

	messageService.readSentMsgs().then(function(response) {
		$scope.listSentMsgs = response.data;
	});
}]);
