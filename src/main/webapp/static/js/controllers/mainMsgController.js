'use strict';
angular.module('messageModule').controller('mainMsgController',  ['$scope', 'messageService', 
									function($scope, messageService) {

	
	messageService.getUsers().then(function(response) {
		$scope.listUsers = response.data;
	});
	
	$scope.welcomeMessage = "Message (Single-Page-App)"
	$scope.instructions="Get started using the available Options"
	
	var socket = new SockJS('/helloworld/notify');
	var stompClient = Stomp.over(socket);
	var senderName = $('#sender').val();
	
	
	var callback = function () {  
		stompClient.subscribe('/topic/notify-'+senderName,  function(message){            
            messageService.readSentMsgs().then(function(response) {
        		$scope.listSentMsgs = response.data;
        	});
        	
            messageService.readReceivedMsgs().then(function(response) {
        		$scope.listRecvdMsgs = response.data;
        	});
        	
        	$scope.$evalAsync( function() { $scope.listSentMsgs, $scope.listRecvdMsgs });
        	
        	$scope.inboxCount = message.body;
            
            return { count: message }
        }); 
	}

	var setup = function () {
	    stompClient.connect({}, function(frame) {
	        callback();
	    });
		stompClient.debug = function(str){};
	}
	
	setup();
}]);
