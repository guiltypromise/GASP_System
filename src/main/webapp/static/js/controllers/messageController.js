'use strict';
angular.module('messageModule').controller('messageController',  ['$scope', '$filter', 'messageService', 
									function($scope, $filter, messageService) {

	messageService.getUsers().then(function(response) {
		$scope.listUsers = response.data;
	});
	
	messageService.readSentMsgs().then(function(response) {
		$scope.listSentMsgs = response.data;
	});
	
	messageService.readReceivedMsgs().then(function(response) {
		$scope.listRecvdMsgs = response.data;
	});
	
	// ------------------------------------ after initial GET requests for loading page

	var socket = new SockJS('/helloworld/notify');
	var stompClient = Stomp.over(socket);
	var senderName = $('#sender').val();
	
	var callback = function () {  
		stompClient.subscribe('/topic/notify-'+senderName,  function(message){            
            messageService.readSentMsgs().then(function(response) {
        		$scope.listSentMsgs = response.data;
        	});
        	
            messageService.readReceivedMsgs().then(function(response) {
        		$scope.listRecvdMsgs = response.data;
        	});
        	
        	$scope.$evalAsync( function() { $scope.listSentMsgs, $scope.listRecvdMsgs});
        	
        	$scope.inboxCount = message.body;
            
            return { count: message }
        }); 
	}

	var setup = function () {
	    stompClient.connect({}, function(frame) {
	        callback();
	    });
	}
	
	setup();
	
	// ------------------------------------- after STOMP messaging
	
	$scope.showUserList = true;
	
	$scope.sendMsg = function(token) {
		
		var senderName = $('#sender').val();
		var token = $("meta[name='_csrf']").attr("content");
		var header = $('#csrfHeader').val();
		
		var dataObj = {  
			"sender": senderName, 
			"recipient": $scope.recipient, 
			"subject": $scope.subject,
			"message": $scope.message, 
			_csrf: token
		}

		messageService.sendMsg( dataObj, token ).then( function(response) {
			if( response.data == 'fail' ) {
				$scope.sentSuccess = false;
			} else {
				$scope.sentSuccess = true;
				$scope.recipient = "";
				$scope.subject = "";
				$scope.message = "";
			}
		});	
	}
	
	$scope.selectUser = function(selectedUsername) {
		$scope.recipient = selectedUsername;
		$scope.showUserList = false;
		$scope.$apply;
	}
	
	$scope.search = function(value) {
		$scope.showUserList = true;

		// value contains partial match
		if( value != null && value.length > 0 ) {
			var found = $filter('searchUserFilter')($scope.listUsers, value );
			$scope.results = found;
		}
		else {
			$scope.results = null;
		}

		$scope.$apply;
	}
	
	$scope.readMessage = function (id) {
		messageService.readMsg(id);
	}
}]);
