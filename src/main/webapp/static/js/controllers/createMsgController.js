'use strict';
angular.module('messageModule').controller('createMsgController',  ['$filter', 'messageService',  
									function( $filter, messageService) {
	
	var vm = this;
	
	vm.init = function () {
		vm.showUserList = true;
		messageService.getUsers().then(function(response) {
			vm.listUsers = response.data;
		});
	}
	
	vm.showUserList = true;
	
	vm.sendMsg = function() {
		
		var senderName = $('#sender').val();
		var token = $("meta[name='_csrf']").attr("content");
		
		var dataObj = {  
			"sender": senderName, 
			"recipient": vm.recipient, 
			"subject": vm.subject,
			"message": vm.message, 
		}

		messageService.sendMsg( dataObj, token ).then( function(response) {
			if( response.data == 'fail' ) {
				vm.sentSuccess = false;
			} else {
				vm.sentSuccess = true;
				vm.recipient = "";
				vm.subject = "";
				vm.message = "";
			}
		});	
	}
	
	vm.selectUser = function(selectedUsername) {
		vm.recipient = selectedUsername;
		vm.showUserList = false;
	}
	
	vm.search = function(value) {
		vm.showUserList = true;

		// value contains partial match
		if( value != null && value.length > 0 ) {
			var found = $filter('searchUserFilter')(vm.listUsers, value );
			vm.results = found;
		}
		else {
			vm.results = null;
		}
	}
	
	vm.init();
}]);
