'use strict';
angular.module('messageModule').controller('inboxMsgController',  ['$scope', '$translate', 'messageService', 
									function($scope, $translate, messageService) {

	messageService.readReceivedMsgs().then(function(response) {
		$scope.listRecvdMsgs = response.data;
	});
	

	$scope.readMessage = function ( msg ) {
		messageService.readMsg(msg.messageID);
		msg.isNew = false;
	};
}]);
