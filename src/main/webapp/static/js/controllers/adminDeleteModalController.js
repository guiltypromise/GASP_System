angular.module('adminModule').controller('adminDeleteModalController', 
		[ 'adminService', '$uibModalInstance', '$log', 'listUsers', 'user',
		function(adminService, $uibModalInstance, $log, listUsers, user ) {
			
		var vm = this;
		
		vm.listUsers = listUsers;
		vm.user = user;
		vm.deleteEnable = false;
		
		// Delete the user from database
		vm.deleteAccount = function (user) {
			listUsers.splice(listUsers.indexOf(user), 1); // STUB delete on JSP view
			adminService.deleteUser( user.username );
		}
		
		vm.confirmDelete = function( message ) {
			if( message.toLowerCase() == 'delete') {
				vm.deleteEnable = true;
			}
			else {
				vm.deleteEnable = false;
			}
		}
		
		vm.deleteOk = function() {
			$log.info("click OK");
			vm.deleteAccount(vm.user);
			$uibModalInstance.close();
		}
		
		vm.cancel = function() {
			$uibModalInstance.close();
		}
		
}]);