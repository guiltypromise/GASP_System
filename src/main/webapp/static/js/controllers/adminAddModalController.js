angular.module('adminModule').controller('adminAddModalController', 
		[ 'adminService', '$uibModalInstance', '$log', 'listUsers',
		function(adminService, $uibModalInstance, $log, listUsers ) {
			
		var vm = this;
		
		vm.listUsers = listUsers;
		vm.roles = ["ADMIN", "USER"];
		
		function isEmpty( str ){
			return( !str|| 0 === str.length)
		}
		
		// Delete the user from database
		vm.addAccount = function (user ) {
			var token = $("meta[name='_csrf']").attr("content");
			adminService.addUser( user, token );
			listUsers.push(user);
		}
		
		vm.validateFirstName = function( firstName ) {
			if( !isEmpty( firstName) ){
				vm.validFirstName = true;
			}
			else {
				vm.validFirstName = false;
			}
			return vm.validFirstName;
		}
		
		vm.validateLastName = function( lastName ) {
			if( !isEmpty( lastName) ){
				vm.validLastName = true;
			}
			else {
				vm.validLastName = false;
			}
			return vm.validLastName;
		}
		
		vm.validatePassword = function( password ) {
			if( !isEmpty( password) && password.length > 5) {
				vm.validPassword = true;
			}
			else {
				vm.validPassword = false;
			}
		}
		
		vm.addUserOk = function() {
			var user = {
				username: vm.username,
				firstName: vm.firstName,
				lastName: vm.lastName,
				email: vm.email,
				enabled: true,
				role: vm.accountRole,
				password: vm.confirmPassword
			};
		    vm.addAccount( user );
			$uibModalInstance.close(false);
		}
		
		vm.cancel = function() {
			$uibModalInstance.close(false);
		}
		
}]);