angular.module('dashboardModule').controller('changePwModalController', 
		[ 'userService', '$uibModalInstance', '$log', 'pvm', 'user',
		function(  userService, $uibModalInstance, $log, pvm, user ) {
			
		var vm = this;
		var token = $("meta[name='_csrf']").attr("content");
		vm.user = user;

		vm.update = function () {
			//quickly validate that the old password entered matches DB password			
			userService.checkUserPass( vm.oldpassword, token ).then(
					function successCallback( response ) {
						$log.info(response);
					if( response.found ) {
						$log.info("Preparing to update user password");
						userService.updatePw( vm.oldpassword, vm.confirmPassword, token);
						$uibModalInstance.close(true);

					}
					else {
						$log.error("Given old password does not match");
						$uibModalInstance.close(false);
					}
			});
		}
		
		vm.changePassOk = function() {
			vm.update( user );
		}
		
		vm.cancel = function() {
			$uibModalInstance.close('cancel');
		}
		
}]);