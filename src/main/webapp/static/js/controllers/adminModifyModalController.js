angular.module('adminModule').controller('adminModifyModalController', 
		[ 'adminService', '$uibModalInstance', '$log', 'user',
		function(adminService, $uibModalInstance, $log, user ) {
			
		var vm = this;

		vm.user = user;
		
		// Delete the user from database
		vm.updateAccount = function (user) {
			$log.info("adminModifyModalController: Updating account record");
		}
		
		vm.deleteOk = function() {
			$log.info("click OK");
			vm.updateAccount(vm.user);
			$uibModalInstance.close();
		}
		
		vm.cancel = function() {
			$uibModalInstance.close();
		}
		
}]);