'use strict';
angular.module('adminModule').controller(
		'adminController', 
		[ 'adminService', '$uibModal', '$log',
		function(adminService, $uibModal, $log ) {

			
	var vm = this;
	
	vm.init = function () {
		vm.deleteSafety = false;
		vm.animationsEnabled = true;
		
		vm.bSortFirstName = false;
		vm.bSortLastName = false;
		vm.bSortEmail = false;
		vm.bSortUsername = false;
		vm.bSortRole = false;
		vm.bSortStatus = false;
		
		vm.bSortReverse = false;
		vm.orderByField = "";
	}
	
	// sort first name
	vm.sortFirstName = function() {
		vm.bSortFirstName = true; // set active
		vm.orderByField = "firstName";
		
		// set the rest inactive
		vm.bSortLastName = false;
		vm.bSortEmail = false;
		vm.bSortUsername = false;
		vm.bSortRole = false;
		vm.bSortStatus = false;
	}
	
	// sort last name
	vm.sortLastName = function () {
		vm.bSortLastName = true;
		vm.orderByField = "lastName";
		
		vm.bSortFirstName = false;
		vm.bSortEmail = false;
		vm.bSortUsername = false;
		vm.bSortRole = false;
		vm.bSortStatus = false;
	}
	
	// sort email
	vm.sortEmail = function () {
		vm.bSortEmail = true;
		vm.orderByField = "email";
		
		vm.bSortFirstName = false;
		vm.bSortLastName = false;
		vm.bSortUsername = false;
		vm.bSortRole = false;
		vm.bSortStatus = false;
	}
	// sort username
	vm.sortUsername = function () {
		vm.bSortUsername = true;
		vm.orderByField = "username";
		
		vm.bSortFirstName = false;
		vm.bSortLastName = false;
		vm.bSortEmail = false;
		vm.bSortRole = false;
		vm.bSortStatus = false;
	}
	// sort role
	vm.sortRole = function () {
		vm.bSortRole = true;
		vm.orderByField = "role";
		
		vm.bSortFirstName = false;
		vm.bSortLastName = false;
		vm.bSortEmail = false;
		vm.bSortUsername = false;
		vm.bSortStatus = false;
	}
	// sort status
	vm.sortStatus = function () {
		vm.bSortStatus = true;
		vm.orderByField = "enabled";
		
		vm.bSortFirstName = false;
		vm.bSortLastName = false;
		vm.bSortEmail = false;
		vm.bSortUsername = false;
		vm.bSortRole = false;
	}
	
	
	vm.sortReverse = function() {
		vm.bSortReverse = !vm.bSortReverse;
	}
			
	// Load user table
	adminService.getUsers().then(function(response) {
			vm.listUsers = response.data;
	});
	
	//Toggle user account status
	vm.toggleAccountStatus = function (user) {
		$log.info(JSON.stringify(user));
		if ( user.enabled ) { 
			user.enabled = false;
		} else {
			user.enabled = true;
		}
		
		adminService.toggleUserStatus( user.username );
	}
	
	
	vm.openDeleteConfirm = function( user ) {
		 var modalInstance = $uibModal.open({
	          animation: vm.animationsEnabled,
	          templateUrl: 'templates/admin/modals/deleteUserModal.jsp',
	          controller: 'adminDeleteModalController',
	          controllerAs: 'vm',
	          resolve: {
	        	  listUsers: function() { return vm.listUsers },
	        	  user: function() { return user },
	          }
	     });
		 modalInstance.result.then(function () {
	          $log.info('Modal dismissed at: ' + new Date());
	     }, function() {});
	}
	
	vm.openAddUserConfirm = function() {
		 var modalInstance = $uibModal.open({
	          animation: vm.animationsEnabled,
	          templateUrl: 'templates/admin/modals/addUserModal.jsp',
	          controller: 'adminAddModalController',
	          controllerAs: 'vm',
	          resolve: {
	        	  listUsers: function() {
	        		  return vm.listUsers 
        		  },
	          }
	     });
		 modalInstance.result.then(function () {
	          $log.info('Modal dismissed at: ' + new Date());
	     }, function() {});
	}
	
	vm.openModifyUser = function( user ) {
		 var modalInstance = $uibModal.open({
	          animation: vm.animationsEnabled,
	          templateUrl: 'templates/admin/modals/modifyUserModal.jsp',
	          controller: 'adminModifyModalController',
	          controllerAs: 'vm',
	          resolve: {
	        	  user: function() { return user }, 
       		  },
	     });
		 modalInstance.result.then(function () {
	          $log.info('Modal dismissed at: ' + new Date());
	     });
	}
	
	vm.init();
}]);