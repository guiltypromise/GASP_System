'use strict';
angular.module('dashboardModule').controller("editProfileController",  
		[ '$window', '$timeout', '$uibModal', '$log', 'userService', 'editProfileService', 
			function( $window, $timeout,  $uibModal, $log, userService, editProfileService) {
			
	
	var pvm = this;
	var token = $("meta[name='_csrf']").attr("content");

	pvm.init = function () {
		userService.getCurrentUser().then( function success( response){
			pvm.user = response.data;
			console.log("User: " + pvm.user);

		});
		pvm.status = false;
		pvm.pwStatus = false;
		pvm.saved = false;

	}
	
	pvm.sendEdit = function() {
		
		var username = $('#username').val();
		var firstname = $('#firstname').val();
		var lastname = $('#lastname').val();
		var email = $('#email').val();
		var token = $('#csrfToken').val();

		
		var actionURL = 'editProfile.htm';
		var dataObj = {  
				"username": username, 
				"firstname": firstname, 
				"lastname": lastname,
				"email": email,
		}
		
		editProfileService.submitProfile( dataObj, token, actionURL ).then( function successCallback(response){
			var user = response.data;
			pvm.user.firstName = user.firstName;
			pvm.user.lastName = user.lastName;
			pvm.user.username = user.username;
			pvm.email = user.email;
			pvm.status = true;
			pvm.changesSaved = true;
			pvm.alertClass = "alert alert-success";

			$timeout( function() {
				 pvm.status = false;
				 pvm.changesSaved = false;
			}, 3000);
		});
		
	}
	
	
	pvm.openChangePwForm = function() {
		 var modalInstance = $uibModal.open({
	          animation: pvm.animationsEnabled,
	          templateUrl: 'templates/dashboard/modals/changePwModal.jsp',
	          controller: 'changePwModalController',
	          controllerAs: 'vm',
	          resolve: {
	        	  user: function() { return pvm.user },
	        	  pvm: function () { return pvm },
	          }
	     });
		 modalInstance.result.then(function (result) {
			 pvm.pwStatus = true;
			 if( result == true ){
				pvm.saved = true;
				pvm.passwordSaved = true;
				pvm.alertClass = "alert alert-success";
			 }
			 else if( result == false ) {
					pvm.saved = false;
					pvm.passwordNotSaved = true;
					pvm.alertClass = "alert alert-danger";
			 }
			 else {
				 pvm.pwStatus = false;
			 }
			 $timeout( function() {
				pvm.pwStatus = false;
				pvm.passwordSaved = false;
				pvm.passwordNotSaved = false;
				 }, 3000);
			 $log.info("Modal result: " + result);
			 $log.info('Modal dismissed at: ' + new Date());
	     });
	}
	
	pvm.init();
 }]);