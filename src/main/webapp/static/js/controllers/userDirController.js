'use strict';
angular.module('spaModule').controller("userDirController",
		['$scope', '$translate', 'userService', function($scope, $translate, userService) {
	
	var vm = this;
	
	vm.users = [];
	
	vm.init = function () {
		userService.getUsers().then( function( response ){  
			vm.users = response.data;	
		});
	};
  
	vm.init();
}]);