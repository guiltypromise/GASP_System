'use strict';
angular.module('spaModule').controller("infoStubController", ['$scope', '$translate', function($scope, $translate) {
	
	$scope.message = 'Prototyping Page';
	$scope.welcome = $translate.instant('welcome.message');
  
}]);