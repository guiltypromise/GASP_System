angular.module('myApp').controller('headerController',  
		['$scope', '$filter', '$log', 'messageService', 'searchService', function($scope, $filter, $log, messageService, searchService) {

	var socket = new SockJS('/helloworld/notify');
	var stompClient = Stomp.over(socket);
	var senderName = $('#sender').val();
	var vm = this;
	vm.showUserList = true;
	
	messageService.getUsers().then(function(response) {
		$scope.listUsers = response.data;
	});
	
	var callback = function () {  
		stompClient.subscribe('/topic/notify-'+senderName,  function(message){
			$scope.msgBadge = message.body;
            $scope.$apply( function() { $scope.msgBadge });

            return { count: message }
        }); 
	}

	var setup = function () {
	    stompClient.connect({}, function(frame) {
            callback();
	    });
	}
	
	setup();
	
	messageService.getMsgCount(senderName).then(function(response){
		var msgs = response.data;
		$scope.msgBadge = msgs;
	});	
	
	$scope.search = function(value) {
		$scope.showUserList = true;

		// value contains partial match
		if( value != null && value.length > 0 ) {
			var found = $filter('searchUserFilter')($scope.listUsers, value );
			$scope.results = found;
		}
		else {
			$scope.results = null;
		}
	}
	
	$scope.selectUser = function( user ) {	
		$log.info( "Selected: " + user.username );
		vm.showUserList = false;
		vm.recipient = user.username;
	}
//	
//	$scope.searchQuery = function( value ) {
//		searchService.searchUser( value );
//	}

}]);