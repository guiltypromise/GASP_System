'use strict';
angular.module('myApp').controller('fileController',  [ '$log', 'fileService',  
	function( $log, fileService) {
	
	var vm = this;
	
	var uploadUrl = "upload";
	var profilePicUrl = "getProfilePic"
	var token = $("meta[name='_csrf']").attr("content");
	
	vm.init = function() {
		vm.getProfilePic();
	}
	vm.uploadFile = function () {
		vm.isError = false;
		vm.isLoading = true;
		$log.info(vm.myFile );
		fileService.uploadFile(vm.myFile, uploadUrl, token).then( function success(response) {
			vm.getProfilePic()
		}, function error( response ) {
			vm.isError = true;
			vm.isLoading = false;

		});
	}
	
	vm.getProfilePic = function () {
		fileService.getProfilePic( profilePicUrl ).then( function successCallback( response ) {
			var user = response.data;
			$log.info(user.avatarPath);
			vm.imageName = user.avatarPath;
			vm.imageSource = user.avatarPath;
		}, function error(response){ // error catch
			vm.isError = true;
		}).finally( function () { // reset loading gif
			vm.isLoading = false;
		});
	}
	
	vm.getValue = function( int ){
		if( int == '3' ) {
			$log.info("found 3");
			return "1";

		}
		else {
			return "0";
		}
	}
	
	vm.init();
}]);