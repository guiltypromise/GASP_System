'use strict';
angular.module("messageModule", ['myApp']);

angular.module('messageModule')
.config(['$translateProvider', '$stateProvider', '$urlRouterProvider', 
	function($translateProvider, $stateProvider) {
	
	
	$translateProvider.useSanitizeValueStrategy('sanitizeParameters');
	$translateProvider.preferredLanguage('en');
	
	$stateProvider.state({
		name: 'createMsg',
	    url: '/createMsg',
	    templateUrl: 'templates/messageSpa/createMsg.jsp',
	    controller: 'createMsgController',
	    controllerAs: 'vm',
	});
	$stateProvider.state({
		name: 'inboxMsg',
	    url: '/inboxMsg',
	    templateUrl: 'templates/messageSpa/inboxMsg.jsp',
	});
	$stateProvider.state({
		name: 'sentMsg',
	    url: '/sentMsg',
	    templateUrl: 'templates/messageSpa/sentMsg.jsp',
	});
	
	// translation table stub until translateProvider can load via URL
	var translations = {

			'messagePage.create.header':"New Message",
			'messagePage.create.sendTo':"Send To: ",
			'messagePage.create.subject':"Subject: ",
			'messagePage.create.body':"Message: ",
			'messagePage.create.sendTo.placeholder':"Enter recipient's name. Partial matching will work with first/last names or usernames.",
			'messagePage.create.subject.placeholder':"Enter subject here.",
			'messagePage.create.body.placeholder':"Enter message body here.",
			'messagePage.create.button.send':"Send",

			'messagePage.inbox.header':"Inbox",
			'messagePage.inbox.from':"From: ",
			'messagePage.inbox.subject':"Subject: ",
			'messagePage.inbox.body':"Message: ",

			'messagePage.sent.header':"Sent Messages",
			'messagePage.sent.recipient':"Sent To:",
			'messagePage.sent.subject':"Subject:",
			'messagePage.sent.body':"Message:",

			'messagePage.confirm.header':"Send Confirmation",
			'messagePage.confirm.success':"Your message has been sent.",
			'messagePage.confirm.error':"Failed to send message.",
			'messagePage.confirm.error.info':"Recipient not found: ",
			'messagePage.confirm.button.close':"Close", 
	};

	  // add translation table
	  $translateProvider
	  	.translations('en', translations)
	    .fallbackLanguage('en')
	    .preferredLanguage('en');
}]);


