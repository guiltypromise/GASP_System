
angular.module("myApp", [
		'adminModule', 
		'dashboardModule', 
		'spaModule', 
		'messageModule', 
		'registerModule', 
		'loginModule', 
		'calendarModule', 
		'ngAnimate',
		'ui.router', 
		'ui.bootstrap', 
		'pascalprecht.translate'
		])
.config([
	'$translateProvider', 
	'$stateProvider', 
	'$urlRouterProvider', 
	'$locationProvider',
    function( $translateProvider, $stateProvider, $urlRouterProvider, $locationProvider, userService ) {
	
		
		
/*	$stateProvider.state( 'login', { 
			name: 'login',
		    url: '/login',
		    controller: 'loginController',
		    controllerAs: 'vm',
		    templateUrl: 'templates/login/login.jsp',
		    authenticate: false,
	});
	$stateProvider.state( 'register', { 
		name: 'register',
	    url: '/register',
	    templateUrl: 'templates/register/register.jsp',
	    authenticate: false,
	});	
	$stateProvider.state( 'registerConfirm', { 
		name: 'registerConfirm',
	    url: '/registerConfirm',
	    templateUrl: 'templates/register/registerConfirm.jsp',
	    authenticate: false
	});	
	$stateProvider.state( 'admin', { 
			name: 'admin',
		    url: '/admin',
		    templateUrl: 'templates/admin/admin.jsp',
		    authenticate: true
		    
	});
	$stateProvider.state('messages', {
			name: 'messages',
		    url: '/messageSpa',
		    templateUrl: 'templates/messageSpa/messageSpa.jsp',
		    authenticate: true
	});
	$stateProvider.state( 'dashboard',{
			name: 'dashboard',
		    url: '/dashboardSpa',
		    templateUrl: 'templates/dashboard/dashboard.jsp',
		    authenticate: true
	});
	$stateProvider.state( 'utility',{
		name: 'utility',
	    url: '/utility',
	    templateUrl: 'templates/spaDemo/spa.jsp',
	    authenticate: true
	});	
	
	
	$stateProvider.state('accessDenied',{
		name: 'accessDenied',
	    url: '/accessDenied',
	    //templateUrl: 'templates/index.jsp',
	    templateUrl: 'templates/errors/accessDenied.jsp',
	    
	});
	$stateProvider.state('welcome',{
		name: 'index',
	    url: '/',
	    //templateUrl: 'templates/index.jsp',
	    templateUrl: 'templates/home/home.jsp',
	    
	});
	$urlRouterProvider.otherwise('/');*/
	
}]);


