angular.module('myApp').directive('userEmailExist', ['userService', function (userService) {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) { // link to register and update the DOM with AJAX calls
        	
        	// custom on Blur since angular has no native onChange support
            if (!ctrl) return; // ctrl is a reference to a controller
            elm.bind('blur', function () {
                scope.$apply(checkUserEmail); // wrap $apply around to execute the function and update bindings
            });

            var checkUserEmail = function () { // anonymous function in variable
            	
                var userEmail = elm.val(); // grab the value from the form input
                // call userService's checkUserEmai; with the email input for AJAX call
                userService.checkUserEmail(userEmail).then(function (result) {
                	
                	// if the response was OK, send back the flag to the registration form view
                    if (result.code == 200) {
                        ctrl.$setValidity('userEmailExist', !result.found); // negation of success
                    }
                });
               
                return userEmail; // return the message
            };
        }
    };
}]);
