angular.module('myApp').directive('userNameExist', ['userService', function (userService) {

    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            if (!ctrl) return;
            elm.bind('blur', function () {
                scope.$apply(checkUserName);
            });
            var checkUserName = function () {
                var userName = elm.val();
                //console.log( "userName = " + userName);
                userService.checkUserName(userName).then(function (result) {
                	//console.log( "result: " + result.found);
                    if (result.code == 200) {
                    	// set validation form status with custom error key
                        ctrl.$setValidity('userNameExist', !result.found); // negation of success
                    }
                });
                return userName;
            };
        }
    };
}]);
