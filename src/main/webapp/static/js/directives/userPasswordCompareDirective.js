angular.module('myApp').directive( 'userPasswordCheck', [ function() { 

	return {
	    //restrict: 'A', // only activate on element attribute
		require: '?ngModel',
        scope: {
            userPasswordCheck: '='
        },
		link: function( scope, elem, attrs, ctrl) {
            ctrl.$validators.userPasswordCheck = function( modelVal, viewVal ) {
            	return modelVal === scope.userPasswordCheck;
            };
            
            
            // take advantage of angularjs's listener for real-time data changes
            scope.$watch('userPasswordCheck', function( newVal, oldVal ) {
            	ctrl.$validate();
            } );
		}
	}
}]);