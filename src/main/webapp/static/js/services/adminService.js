'use strict';
angular.module('adminModule').factory('adminService', ['$http', '$q', function($http, $q) {
	
	var getUsers = function () {
		return $http.get('adminListUsers.htm');
	};
	
	var toggleUserStatus = function(username) {
		return $http.get('toggleUserStatus', {
		    params: { userName: username }
		})
	};
	
	var deleteUser = function( username ) {
		return $http.get('deleteUserAccount', {
		    params: { userName: username }
		})	
	};
	
	var addUser = function( dataObj, token ) {
		return $http({
			   method  :'POST',
			   url:'addUser.htm',
			   data: $.param( dataObj ), // pass in data as jQuery params instead of json
			   dataType: 'x-www-form-urlencoded',
			   headers :{'Content-Type':'application/x-www-form-urlencoded', 'X-CSRF-TOKEN': token}, // set the headers so angular passing info as form data (not request payload)
		});
	}
	
	return {
		getUsers: getUsers,
		toggleUserStatus: toggleUserStatus,
		deleteUser: deleteUser,
		addUser: addUser,
	}
	
}]);

