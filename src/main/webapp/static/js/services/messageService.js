angular.module('messageModule').service("messageService", [ '$http', function($http) {
	var socket = new SockJS('/helloworld/notify');
	var stompClient = Stomp.over(socket);
	var senderName = $('#sender').val();
	
	var callback = function () {  
		stompClient.subscribe('/topic/notify-'+senderName,  function(message){
            return { count: message }
        }); 
	}

	var setup = function () {
	    stompClient.connect({}, function(frame) {
	        callback();
	    });
	}
	
	var getUsers = function () {
		return $http.get('getUserList.htm');
	}
	
	var readSentMsgs = function () {
		return $http.get('readSentMessages');
	}
	
	var readReceivedMsgs = function () {
		return $http.get('readReceivedMessages');
	}
	
	var readMsg = function( id ) {
		return $http({
			   method  :'GET',
			   url:'readMessage',
			   params: { messageID: id }, // pass in data as jQuery params instead of json
		});	
	}
	
	var sendMsg = function( dataObj, token ) {
		return $http({
			   method  :'POST',
			   url:'sendMessage',
			   data: $.param( dataObj ), // pass in data as jQuery params instead of json
			   dataType: 'x-www-form-urlencoded',
			   headers :{'Content-Type':'application/x-www-form-urlencoded', 'X-CSRF-TOKEN': token}, // set the headers so angular passing info as form data (not request payload)
		});
	}
	
	var getMsgCount = function ( senderName ) {
		return $http(
	    		// send http GET request with userName data to Spring Back End that has mapping for the action URL
		   {   method: 'GET', 
			   url: 'countNewMsg', 
			   params: { user: senderName } 
		   });
	}
	

	return {
		stompClient: stompClient,
		callback: callback,
		getUsers: getUsers,
		readSentMsgs: readSentMsgs,
		readReceivedMsgs: readReceivedMsgs,
		sendMsg: sendMsg,
		readMsg: readMsg,
		getMsgCount: getMsgCount,
	}
}]);