angular.module('dashboardModule').factory('editProfileService', ['$http', function($http){
	
	var submitProfile = function( dataObj, token, actionURL ){
			return $http({
				   method  :'POST',
				   url: actionURL,
				   data: $.param( dataObj ), // pass in data as jQuery params instead of json
				   dataType: 'x-www-form-urlencoded',
				   headers :{'Content-Type':'application/x-www-form-urlencoded', 'X-CSRF-TOKEN': token}, // set the headers so angular passing info as form data (not request payload)
			});
	}
	
	return {
		submitProfile: submitProfile,
	};
}]);