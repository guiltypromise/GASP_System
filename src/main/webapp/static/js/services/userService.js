angular.module('myApp').factory('userService', ['$q', '$http', '$log', '$rootScope',
	function ($q, $http, $log, $rootScope) {
	
	

	
    var checkUserName = function (name) {
        var action = "searchUser.htm"
        	$log.debug( " in search user AJAX ");
        return $http(
        		// send http GET request with userName data to Spring Back End that has mapping for the action URL
            { method: 'GET', url: action, params: { userName: name } })
            .then(
            function (result) {
                return { code: 200, found: result.data == "success" } // returns a JSON object with these attrs
            }, function (result) {
                return { code: 500, found: result.data == "fail"  }
                
            });
    }
    
    var checkUserEmail = function (name) {
        var action = "searchUserEmail.htm"
        return $http(
        		// send http GET request with email data to Spring Back End that has mapping for the action URL
            { method: 'GET', url: action, params: { userEmail: name } })
            .then(
            function (result) { // check the result of the http response
            	// return code 200 for OK and store into attr 'found' the boolean if result.data eq to "success"
                return { code: 200, found: result.data == "success" } // returns a JSON object with these attrs
            }, function (result) {
                return { code: 500, found: result.data == "fail"  }
            });
    }
    
    var checkUserPass = function( oldpassword, token ) {
    	var action = "checkUserPass.htm";
    	return $http(
        		// send http GET request with userName data to Spring Back End that has mapping for the action URL
            { method: 'POST', 
            	url: action, 
            	params: { password: oldpassword },
            	dataType: 'x-www-form-urlencoded',
  			    headers :{'Content-Type':'application/x-www-form-urlencoded', 'X-CSRF-TOKEN': token}, 
            })
            .then(
            function (result) {
                return { code: 200, found: result.data == "valid" } // returns a JSON object with these attrs
            }, function (result) {
                return { code: 500, found: result.data == "fail"  }
            });
    }
    
	var getUsers = function(){
		return $http.get('getUserList.htm');
	}
    
    var getCurrentUser = function () {
    	var action = "getCurrentUser.htm";
    	return $http({
    		method: 'GET',
    		url: action,
    	});
    }
    

    
    var updatePw = function( oldPassword, newPassword, token ) {
		var actionURL = "changePassword.htm";
		var dataObj = {
			newPassword: newPassword,
		    oldPassword: oldPassword
		};
		return $http({
			   method  :'POST',
			   url: actionURL,
			   data: $.param(dataObj), // pass in data as jQuery params instead of json
			   dataType: 'x-www-form-urlencoded',
			   headers :{'Content-Type':'application/x-www-form-urlencoded', 'X-CSRF-TOKEN': token}, // set the headers so angular passing info as form data (not request payload)
		});
	}
    
	var isAuthenticated = function () {
		var action = "authenticate.htm"
			var deferred = $q.defer();
		return $http ({
			method : 'GET',
			url: action,
		}).then( function successCallback(response){
			return deferred.resolve( true );
		}, function error(response){
			return deferred.reject();
		});
	}
	
	var doLogin = function ( user ) {
		var action = "doLogin.htm"
		$log.debug("Checking " + user.username);
			
			
	}

    
    return {
    	
        checkUserName: checkUserName, // return checkUserName http results
        checkUserPass: checkUserPass,
        checkUserEmail: checkUserEmail,
        
        getUsers: getUsers,
        getCurrentUser:getCurrentUser,
        
        updatePw: updatePw,
    	isAuthenticated: isAuthenticated,
    	doLogin: doLogin,

    };
}]);