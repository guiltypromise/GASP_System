angular.module('myApp').service("searchService", function($http, $q) {
	var searchUser = function (value) {
		console.log("Search for: " + value );
		return $http({
			   method: 'GET',
			   url: 'searchUserPage',
			   //params: { query: value }, // pass in data as jQuery params instead of json
		});
		
	}
	
	return {
		searchUser: searchUser,
	}
});