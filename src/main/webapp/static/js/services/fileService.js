angular.module('myApp').service('fileService', ['$http', function ($http) {
	
	
    var uploadFile = function(file, uploadUrl, token){
        var fd = new FormData();
        fd.append('file', file);
        return $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined, 'X-CSRF-TOKEN': token}
        });
    }
    
    var getProfilePic = function( actionURL ) {
      return $http({
    	method: 'GET',
    	url: actionURL,
      });
    }
    return {
    	uploadFile:uploadFile,
    	getProfilePic: getProfilePic,
    }
}]);