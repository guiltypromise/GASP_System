<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="modal-header">
  <h3 class="modal-title">Delete Confirmation</h3>
</div>
<div class="modal-body">
	
	<div class="alert alert-danger">
 	 	You are about to delete the following user account:
	</div>
	<table class="table table-sm table-bordered">
		<tbody>
			<tr>
				<th scope="row"><spring:message code="dashboard.profile.user"/> </th>
				<td id="view-userName">{{vm.user.username}}</td>
			</tr>
			<tr>
				<th scope="row"><spring:message code="dashboard.profile.firstName"/> </th>
				<td id="view-firstName"> {{vm.user.firstName}} </td>
			</tr>
			<tr>
				<th scope="row"><spring:message code="dashboard.profile.lastName"/></th>
				<td id="view-lastName">{{vm.user.lastName }}</td>
			</tr>
			<tr>
				<th scope="row"><spring:message code="dashboard.profile.email"/></th>
				<td id="view-email">{{vm.user.email}}</td>
			</tr>
			<tr>
				<th scope="row"><spring:message code="dashboard.profile.accountType"/></th>
				<td>{{ vm.user.role }}</td>
			</tr>
		</tbody>
	</table>
	<p>Click 'Cancel' if you are not sure or enter in 'Delete' to continue.</p>
	
		<input type="text" id="confirmMessage"
		name="confirmMessage"
		placeholder="Type 'Delete' to enable Confirm Delete"
		autocomplete="off" 
		data-ng-model="vm.confirmMessage"
		data-ng-change="vm.confirmDelete(vm.confirmMessage)">
							
</div>
<div class="modal-footer">
  <button class="btn btn-primary" type="button" ng-click="vm.cancel()">Cancel</button>
  <button class="btn btn-danger" type="button" ng-disabled="!vm.deleteEnable" ng-click="vm.deleteOk()">Confirm Delete</button>
</div>