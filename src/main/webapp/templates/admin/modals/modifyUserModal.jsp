<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="modal-header">
  <h3 class="modal-title"> Modify User Account </h3>
</div>
<form id="modifyUserForm" name="modifyUserForm"
class="form-horizontal" method="post">
	<div class="modal-body">
		<div class="alert alert-info">
 	 		<strong>Instructions:</strong> Fill in all fields completely to enable 'Modify' option.
		</div>
		<fieldset>
		<br>

		</fieldset>
	</div>

	<div class="modal-footer">
	  <button id="admin_Button_ModalCancelAddUser" class="btn btn-primary" type="button" data-ng-click="vm.cancel()">Cancel</button>
	  <button id="admin_Button_ModalOkAddUser" class="btn btn-success" type="button" 
	  data-ng-disabled="modifyUserForm.username.$error.userNameExist || modifyUserForm.email.$error.userEmailExist
										|| modifyUserForm.confirmPassword.$error.userPasswordCheck || modifyUserForm.confirmPassword.$untouched
										|| modifyUserForm.username.$invalid
										|| modifyUserForm.email.$invalid
										|| modifyUserForm.firstname.$invalid
										|| modifyUserForm.lastname.$invalid
										|| modifyUserForm.accountType.$invalid" 
	  			data-ng-click="vm.addUserOk()">Save Changes</button>
	</div>
</form>
