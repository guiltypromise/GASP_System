<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="modal-header">
  <h3 class="modal-title"> Add User Account </h3>
</div>
<form id="addUserForm" name="addUserForm"
class="form-horizontal" method="post">
	<div class="modal-body" data-ng-cloak>
		<div class="alert alert-info">
 	 		<strong>Instructions:</strong> Fill in all fields completely to enable 'Add User' option.
		</div>
		<fieldset>
		<br>
				<!-- Text input-->
				<div class="form-group"			
					data-ng-class="{ 'has-success' : addUserForm.username.$valid && !addUserForm.username.$error.userNameExist
				, 'has-error' : addUserForm.username.$error.userNameExist }">
					<label class="col-md-4 control-label" for="username"><spring:message code="dashboard.profile.user"/></label>

					<div class="col-md-5">

						<input id="username" name="username" type="text"
							autofocus
							placeholder="Ex: johndoe" class="form-control input-md"
							required data-user-name-exist
							data-ng-model-options="{ updateOn: 'blur' }"
							data-ng-model="vm.username">

					</div>
					<p data-ng-cloak data-ng-show="addUserForm.username.$error.userNameExist"
						class="help-block"><spring:message code="dashboard.profile.edit.username.error"/></p>
					<p  data-ng-cloak data-ng-show="!addUserForm.username.$invalid && !addUserForm.username.$error.userNameExist"
						class="help-block"><spring:message code="dashboard.profile.edit.username.available"/></p>
				</div>


				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="firstname"><spring:message code="dashboard.profile.firstName"/></label>
					<div class="col-md-5">
						<input id="firstname" name="firstname" type="text"
							placeholder="Ex: John" class="form-control input-md" required
							data-ng-blur="vm.validateFirstName( vm.firstName )"
						    data-ng-model="vm.firstName">
					</div>
					<span class="fa fa-check fa-2x checkmark-span-green" data-ng-show="vm.validFirstName"></span>
				</div>

				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="lastname"><spring:message code="dashboard.profile.lastName"/></label>
					<div class="col-md-5">
						<input id="lastname" name="lastname" type="text" required
							placeholder="Ex: Doe" class="form-control input-md"
							data-ng-blur="vm.validateLastName( vm.lastName )"
							data-ng-model="vm.lastName">

					</div>
					<span class="fa fa-check fa-2x checkmark-span-green" data-ng-show="vm.validLastName"></span>
					
				</div>

				<!-- Email input-->
				<div class="form-group"
					data-ng-class="{ 'has-success' : addUserForm.email.$valid && !addUserForm.email.$error.userEmailExist
					, 'has-error' : addUserForm.email.$error.userEmailExist }">
					<label class="col-md-4 control-label" for="email"><spring:message code="dashboard.profile.email"/></label>
					<div class="col-md-5">
						<input id="email" name="email" type="email"
							required
							placeholder="Ex: johndoe@outlook.com"
							class="form-control input-md" data-user-email-exist
							data-ng-model-options="{ updateOn: 'blur' }"
							data-ng-model="vm.email">
					</div>
					<p data-ng-cloak data-ng-show="addUserForm.email.$error.userEmailExist"
						class="help-block"><spring:message code="dashboard.profile.edit.email.error"/></p>
					<p data-ng-cloak data-ng-show="!addUserForm.email.$invalid && !addUserForm.email.$error.userEmailExist"
						class="help-block"><spring:message code="dashboard.profile.edit.email.available"/></p>

				</div>
				
				<!--  Account Type -->
				<div class="form-group">
					<label class="col-md-4 control-label" for="accountType">Account Type: </label>
					<div class="col-md-5">
						<select id="select-account-type" class="col-md-2" name="accountType" required data-ng-model="vm.accountRole" data-ng-options="type for type in vm.roles"></select>
					</div>
					<span class="fa fa-check fa-2x checkmark-span-green" data-ng-show="vm.accountRole"></span>
					
				</div>
				
				<!-- Password input-->
			<div class="form-group" 
				data-ng-class="{ 'has-error' : !addUserForm.password.$valid || addUserForm.confirmPassword.$error.userPasswordCheck }">
				<label class="col-md-4 control-label" for="password"><spring:message code='register.form.pass'/>
				</label>
				<div class="col-md-5">
					<input id="password" name="password" type="password"
						placeholder="<spring:message code='register.form.pass.placeholder'/>" class="form-control input-md" required
						data-ng-required data-ng-minlength="6" data-ng-model='vm.password' data-ng-change="vm.validatePassword(vm.password)">
					
				</div>
				<p data-ng-cloak data-ng-show="addUserForm.password.$error.required" class="help-block"><spring:message code='register.form.pass.error.required'/></p>
           		<p data-ng-cloak data-ng-show="addUserForm.password.$error.minlength" class="help-block"><spring:message code='register.form.pass.error.minimum'/></p>
				<span data-ng-cloak class="fa fa-check fa-2x checkmark-span-green" data-ng-show="vm.validPassword"></span>
				
			</div>
			

			<!-- Confirm Password input-->
			<div class="form-group"
				data-ng-class="{ 'has-error' : !addUserForm.confirmPassword.$valid}">
				<label class="col-md-4 control-label" for="confirmPassword"><spring:message code='register.form.passConfirm'/></label>
				<div class="col-md-5">
					<input id="confirmPassword" name="confirmPassword"
						type="password" placeholder="<spring:message code='register.form.passConfirm.help'/>"
						class="form-control input-md" required
						data-ng-required data-ng-model='vm.confirmPassword' data-user-password-check="vm.password"> 

				</div>
				<p data-ng-cloak data-ng-show="addUserForm.confirmPassword.$error.required" class="help-block"><spring:message code='register.form.passConfirm.error.required'/></p>
				<p data-ng-cloak data-ng-show="addUserForm.confirmPassword.$error.userPasswordCheck && !addUserForm.confirmPassword.$error.required" class="help-block"><spring:message code='register.form.passConfirm.error.noMatch'/></p>
			    <span data-ng-cloak class="fa fa-check fa-2x checkmark-span-green" data-ng-show="vm.validPassword && !addUserForm.confirmPassword.$error.userPasswordCheck && !addUserForm.confirmPassword.$error.required"></span>
			    
			</div>
		</fieldset>
	</div>

	<div class="modal-footer">
	  <button id="admin_Button_ModalCancelAddUser" class="btn btn-primary" type="button" data-ng-click="vm.cancel()">Cancel</button>
	  <button id="admin_Button_ModalOkAddUser" class="btn btn-success" type="button" 
	  data-ng-disabled="addUserForm.username.$error.userNameExist || addUserForm.email.$error.userEmailExist
										|| addUserForm.confirmPassword.$error.userPasswordCheck || addUserForm.confirmPassword.$untouched
										|| addUserForm.username.$invalid
										|| addUserForm.email.$invalid
										|| addUserForm.firstname.$invalid
										|| addUserForm.lastname.$invalid
										|| addUserForm.accountType.$invalid" 
	  			data-ng-click="vm.addUserOk()">Add User</button>
	</div>
</form>
