<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="modal-header">
  <h3 class="modal-title"> Change Password </h3>
</div>
<form id="changePassForm" name="changePassForm"
class="form-horizontal" method="post">
	<div class="modal-body">
		<div class="alert alert-info">
 	 		<strong>Instructions:</strong> Fill in all fields completely to enable 'Save' option.
 	 		<br>
 	 		<br>
 	 		<p>Password Requirements:
 	 			<ul>
 	 				<li>Minimum 6 characters in length</li>
 	 			</ul>
 	 		</p>
		</div>
		<fieldset>
		<br>
			<!-- Password input-->
			<div class="form-group" >
				<label class="col-md-4 control-label" for="password">Old Password:
				</label>
				<div class="col-md-5">
					<input id="oldpassword" name="oldpassword" type="password"
						placeholder="Enter current password" class="form-control input-md" required
						data-ng-required data-ng-minlength="6" data-ng-model='vm.oldpassword'>
					
				</div>
				<p data-ng-cloak data-ng-show="changePassForm.oldpassword.$error.required" class="help-block"><spring:message code='register.form.pass.error.required'/></p>
			</div>
			
			<hr>
			
				<!-- Password input-->
			<div class="form-group" 
				data-ng-class="{ 'has-error' : !changePassForm.password.$valid || changePassForm.confirmPassword.$error.userPasswordCheck }">
				<label class="col-md-4 control-label" for="password">New Password:
				</label>
				<div class="col-md-5">
					<input id="password" name="password" type="password"
						placeholder="Must meet requirements" class="form-control input-md" required
						data-ng-required data-ng-minlength="6" data-ng-model='vm.password'>
					
				</div>
				<p data-ng-cloak data-ng-show="changePassForm.password.$error.required" class="help-block"><spring:message code='register.form.pass.error.required'/></p>
           				<p data-ng-cloak data-ng-show="changePassForm.password.$error.minlength" class="help-block"><spring:message code='register.form.pass.error.minimum'/></p>
			</div>
						<!-- Confirm Password input-->
			<div class="form-group"
				data-ng-class="{ 'has-error' : !changePassForm.confirmPassword.$valid}">
				<label class="col-md-4 control-label" for="confirmPassword"><spring:message code='register.form.passConfirm'/></label>
				<div class="col-md-5">
					<input id="confirmPassword" name="confirmPassword"
						type="password" placeholder="<spring:message code='register.form.passConfirm.help'/>"
						class="form-control input-md" required
						data-ng-required data-ng-model='vm.confirmPassword' data-user-password-check="vm.password"> 

				</div>
				<p data-ng-cloak data-ng-show="changePassForm.confirmPassword.$error.required" class="help-block"><spring:message code='register.form.passConfirm.error.required'/></p>
						<p data-ng-cloak data-ng-show="changePassForm.confirmPassword.$error.userPasswordCheck && !changePassForm.confirmPassword.$error.required" class="help-block"><spring:message code='register.form.passConfirm.error.noMatch'/></p>
			    
			</div>
		</fieldset>
	</div>

	<div class="modal-footer">
	  <button id="admin_Button_ModalCancelAddUser" class="btn btn-primary" type="button" data-ng-click="vm.cancel()">Cancel</button>
	  <button id="admin_Button_ModalOkAddUser" class="btn btn-success" type="button" 
	  			data-ng-disabled="changePassForm.confirmPassword.$error.userPasswordCheck || changePassForm.confirmPassword.$untouched"
	  			data-ng-click="vm.changePassOk()">Save</button>
	</div>
</form>
