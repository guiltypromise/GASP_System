<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>


<div data-ng-controller="calendarDemo" class="ng-scope" data-ng-cloak>
	<div class=row>
		<div class="col-lg-3"></div>
		 <calendar data-selected="day"></calendar>
		 
	</div>

	<span>Selected date: <strong>{{day.format('dddd, MMMM Do YYYY')}}</strong></span>
</div>