<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div data-ng-controller="widgetsController">
	<h1><spring:message code="spaPage.widgets.header"/></h1>
	
	<h3>{{message}}</h3>
	
	<br><br>
</div>
