<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<div id="demo3-container" class="row" data-ng-controller="userDirController as vm">

	<h2> {{'directory.title' | translate}}</h2>
	
	<br>
	
	<div class="row">
		<div class="col-sm-2">
		
		</div>
		<div data-ng-cloak class="col-sm-8">
			<table class="table">
				<thead>
					<tr>	
						<th scope="col">{{ 'directory.table.header.user' | translate }} </th>		
						<th scope="col">{{ 'directory.table.header.firstName' | translate }}</th>
						<th scope="col">{{ 'directory.table.header.lastName' | translate }}</th>
						<th scope="col">{{ 'directory.table.header.status' | translate }}</th>						
					</tr>
				</thead>
				<tbody>
					<tr data-ng-repeat="user in vm.users">
						<td>{{ user.username }}</td>
					    <td>{{ user.firstName }}</td>
					    <td>{{ user.lastName }}</td>
					    <td>
							<span class="label label-success" data-ng-show="user.enabled"><spring:message code="admin.table.label.active"/></span>
							<span class="label label-danger" data-ng-show="!user.enabled"><spring:message code="admin.table.label.inactive"/></span>
					    </td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div>
</div>