
<div id="index-panel" class="panel panel-default" data-ng-controller="indexSpaController">
			<div class="panel-heading">{{welcomeMessage}}</div>
			<div class="panel-body">
				<h1>{{message}}</h1>
				<h3>{{instructions}}</h3>
				<br><br>
			</div>
</div>
