<!-- 
	login.jsp
	
	@author jason.r.martin
 -->
<%@page session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="site-wrapper">
	<div class="site-wrapper-inner">
		<div class="cover-container">
			<div class="inner cover">
				<c:url var="loginUrl" value="#!/login" />
				<form class="form-signin" method="post" action="${loginUrl}"
					name="loginForm" id="loginForm">

					<h2 id="loginTitle"class="form-signin-heading"><spring:message code="login.header"/></h2>
					
					
					<c:if test="${param.error != null}">
						<div class="alert alert-danger">
								<p><spring:message code="login.form.error"/></p>
								<p><c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/></p>
						</div>
					</c:if>
					<c:if test="${ERROR != null}">
						<div class="alert alert-danger">
							<p>
								<c:out value="${ERROR}" />
							</p>
						</div>
					</c:if>

					<label for="inputUsername" class="sr-only"><spring:message code="login.form.user.placeholder"/></label> 
					
					
						<input
						type="text" name="inputUsername" id="inputUsername"
						title="User Name" class="form-control" placeholder='<spring:message code="login.form.user.placeholder"/>'
						required autofocus
						data-ng-model="vm.username"> <label for="inputPassword"
						class="sr-only"><spring:message code="login.form.pass.placeholder"/></label> 
						
						
						<input type="password"
						name="inputPassword" id="inputPassword" title="Password"
						class="form-control" placeholder='<spring:message code="login.form.pass.placeholder"/>' required
						data-ng-model="vm.password"
						>

					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

					<button class="btn btn-lg btn-info btn-block" 
						type="submit" data-ng-click="vm.authenticate(vm.username, vm.password)">
						<spring:message code="login.form.button.signIn"/></button>
						<div class="login-new-account-link"> </div>
					<a id="newAccountLink" data-ui-sref="register"><span
						class="glyphicon glyphicon-pencil"></span> <spring:message code="login.form.createAccount"/></a>
				</form>
			</div>
		</div>
	</div>
</div>