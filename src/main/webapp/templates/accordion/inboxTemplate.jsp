<div  class="panel {{panelClass || 'panel-default'}}">
	<div class="panel-heading">
		<div class="panel-title" style="line-height: 1.1;">
			<a href tabindex="-1" class="accordion-toggle"
				uib-accordion-transclude="heading" ng-click="toggleOpen()">
				
				<div class="row no-margin-bottom fsa-vertical-center__parent">
					<div class="col-lg-11 col-xs-10">
						<span ng-class="{'text-muted': isDisabled}"> 
							{{ heading }}
						</span>
					</div>
					
					<div class="col-lg-1 col-xs-2 fsa-vertical-center__child-right">
						<i class="pull-right panel-arrow"
					   		ng-class="{'fa fa-chevron-down':!status.open, 'fa fa-chevron-up':status.open}"></i>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="panel-collapse collapse" uib-collapse="!status.open">
		<div class="panel-body" ng-transclude></div>
	</div>
</div>