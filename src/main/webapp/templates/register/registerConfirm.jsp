<!-- 
	registerSuccess.jsp
	
	@author jason.r.martin
 -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="site-wrapper">
	<div class="site-wrapper-inner">
		<div class="cover-container">
			<div class="inner cover">
				<div class="well">
					<h1 class="cover-heading"><c:out value ="${REGISTERSTATUSMESSAGE}"/></h1>
				</div>
			</div>
		</div>
	</div>
</div>

