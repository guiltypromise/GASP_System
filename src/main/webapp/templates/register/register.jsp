<!-- 
	register.jsp
	
	@author: jason.r.martin
	
 -->

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="site-wrapper">
	<div class="site-wrapper-inner">
		<div class="cover-container">
			<div data-ng-controller="registerController" data-ng-cloak>
				<form class="form-horizontal" method="post" action="registerForm.htm"
					name="registerForm" id="registerForm">
					<fieldset id="registerFieldSet">

						<!-- Form Name -->
						<legend><spring:message code='register.form.title'/></legend>
						
						<!-- Text input-->
						<div id="registerForm" class="form-group">
							<label class="col-md-4 control-label" for="firstname"><spring:message code='register.form.firstName'/></label>
							<div class="col-md-5">
								<input id="firstname" name="firstname" type="text"
									placeholder="<spring:message code='register.form.firstName.placeholder'/>" class="form-control input-md"
									required autofocus>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="lastname"><spring:message code='register.form.lastName'/></label>
							<div class="col-md-5">
								<input id="lastname" name="lastname" type="text"
									placeholder="<spring:message code='register.form.lastName.placeholder'/>" class="form-control input-md"
									required>

							</div>
						</div>

						<!-- Email input-->
						<div class="form-group"
						data-ng-class="{ 'has-success' : registerForm.email.$valid && !registerForm.email.$error.userEmailExist
								  , 'has-error' : registerForm.email.$error.userEmailExist }">
							<label class="col-md-4 control-label" for="email"><spring:message code='register.form.email'/></label>
							<div class="col-md-5">
								<input id="email" name="email" type="email"
									placeholder="<spring:message code='register.form.email.placeholder'/>" class="form-control input-md" required
									data-user-email-exist data-ng-model-options="{ updateOn: 'blur' }" data-ng-model="registration.email">
								<span class="help-block"><spring:message code='register.form.email.help'/></span>
							</div>
							<p data-ng-cloak data-ng-show="registerForm.email.$error.userEmailExist" class="help-block"> <spring:message code='register.form.email.error'/></p>
							<p data-ng-cloak data-ng-show="!registerForm.email.$invalid && !registerForm.email.$error.userEmailExist" class="help-block"> <spring:message code='register.form.email.available'/></p>
							
						</div>

						<!-- Text input-->
						<div class="form-group" 
							data-ng-class="{ 'has-success' : registerForm.username.$valid && !registerForm.username.$error.userNameExist
									  , 'has-error' : registerForm.username.$error.userNameExist }">
							<label class="col-md-4 control-label" for="username"><spring:message code='register.form.user'/></label>
							
							<div class="col-md-5">
							
									<input id="username" name="username" type="text"
										placeholder="<spring:message code='register.form.user.placeholder'/>" class="form-control input-md" required 
										data-user-name-exist data-ng-model-options="{ updateOn: 'blur' }" data-ng-model="registration.username">
							</div>
							<p data-ng-cloak data-ng-show="registerForm.username.$error.userNameExist" class="help-block"> <spring:message code='register.form.user.error'/></p>
							<p data-ng-cloak data-ng-show="!registerForm.username.$invalid && !registerForm.username.$error.userNameExist" class="help-block"> <spring:message code='register.form.user.available'/></p>
						</div>
						
						
						
						

						<!-- Password input-->
						<div class="form-group" 
							data-ng-class="{ 'has-error' : !registerForm.password.$valid || registerForm.confirmPassword.$error.userPasswordCheck }">
							<label class="col-md-4 control-label" for="password"><spring:message code='register.form.pass'/>
							</label>
							<div class="col-md-5">
								<input id="password" name="password" type="password"
									placeholder="<spring:message code='register.form.pass.placeholder'/>" class="form-control input-md" required
									data-ng-required data-ng-minlength="6" data-ng-model='password'>
								
							</div>
							<p data-ng-cloak data-ng-show="registerForm.password.$error.required" class="help-block"><spring:message code='register.form.pass.error.required'/></p>
              				<p data-ng-cloak data-ng-show="registerForm.password.$error.minlength" class="help-block"><spring:message code='register.form.pass.error.minimum'/></p>
						</div>
						

						<!-- Confirm Password input-->
						<div class="form-group"
							data-ng-class="{ 'has-error' : !registerForm.confirmPassword.$valid}">
							<label class="col-md-4 control-label" for="confirmPassword"><spring:message code='register.form.passConfirm'/></label>
							<div class="col-md-5">
								<input id="confirmPassword" name="confirmPassword"
									type="password" placeholder="<spring:message code='register.form.passConfirm.help'/>"
									class="form-control input-md" required
									data-ng-required data-ng-model='confirmPassword' data-user-password-check="password"> 
									<span class="help-block"><spring:message code='register.form.passConfirm.help'/></span>

							</div>
							<p data-ng-cloak data-ng-show="registerForm.confirmPassword.$error.required" class="help-block"><spring:message code='register.form.passConfirm.error.required'/></p>
  							<p data-ng-cloak data-ng-show="registerForm.confirmPassword.$error.userPasswordCheck && !registerForm.confirmPassword.$error.required" class="help-block"><spring:message code='register.form.passConfirm.error.noMatch'/></p>
						    
						</div>

						<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
						<!-- Button -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="confirmation"></label>
							<div class="col-md-4">
								<button id="confirmation" name="confirmation"
									class="btn btn-primary" 
									data-ng-disabled="registerForm.username.$error.userNameExist || registerForm.email.$error.userEmailExist 
									|| registerForm.confirmPassword.$error.userPasswordCheck || registerForm.confirmPassword.$untouched " 
									><spring:message code='register.form.button.submit'/></button>
							</div>
						</div>

					</fieldset>
				</form>

			</div>
		</div>
	</div>
</div>