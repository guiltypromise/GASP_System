<!-- 
	messages.jsp
	
	@author jason.r.martin
 -->
<%@page session="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="site-wrapper">
	<div class="container">
		<div class="site-inner">

			<div class="row" data-ng-cloak data-ng-controller="mainMsgController">

			<div class="col-md-3">
				<div class="panel panel-info">
					<div class="panel-heading"><spring:message code='messagePage.options.header'/></div>
					<div class="panel-body">
						<ul class="nav nav-pills nav-stacked options">
							<li>
								<%-- Code Review: interactable IDs on all things actionable --%>
								<a id="message_Link_NewMessage"
									href="#!/createMsg"> <span
									class="glyphicon glyphicon-pencil"></span> <spring:message code='messagePage.options.new'/>
								</a>
							</li>
							<li><a id="message_Link_Inbox" href="#!/inboxMsg"> <span
									class="glyphicon glyphicon-inbox"></span> <spring:message code='messagePage.options.inbox'/> 
									<span class="label label-success" data-ng-show="inboxCount > 0">{{ inboxCount }} Unread</span></h4>
							</a></li>
							<li><a id="message_Link_Sent" href="#!/sentMsg"> <span
									class="glyphicon glyphicon-send"></span> <spring:message code='messagePage.options.sent'/>
							</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<%-- Templates routed here --%>
			<div class="col-md-9" data-ui-view></div>
					
			</div>
		</div>
	</div>
</div>