<div id="createMessage">
	<div class="panel panel-default">
		<div class="panel-heading">{{ 'messagePage.create.header' |
			translate }}</div>
		<div class="panel-body">
			<div class="row">
				<form id="messageForm" name="messageForm" class="form-horizontal"
					role="form" method="post">
					<div class="form-group">

						<label class="sender-label col-md-2" for="recipient">{{
							'messagePage.create.sendTo' | translate }}</label>
						<div class="col-md-9">
							<input type="text" class="form-control" id="recipient"
								name="recipient"
								placeholder="{{ 'messagePage.create.sendTo.placeholder' | translate }}"
								autocomplete="off" data-ng-model="vm.recipient"
								data-ng-change="vm.search(vm.recipient)" required>

							<div id="userResultsList" class="list-group"
								data-ng-show="vm.showUserList">
								<a id="userResultsListItem" class="list-group-item"
									data-ng-repeat="recipient in vm.results" data-ng-model="vm.searchValue"
									data-ng-click="vm.selectUser(recipient.username)">
									{{recipient.firstName }} {{ recipient.lastName }}
									({{recipient.username}})</a>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="subject-label col-md-2" for="subject">{{
							'messagePage.create.subject' | translate }}</label>
						<div class="col-md-9">
							<input type="text" class="form-control"
								placeholder="{{ 'messagePage.create.subject.placeholder' | translate }}"
								autocomplete="off" name="subject" id="subject"
								data-ng-model="vm.subject" required>
						</div>
					</div>
					<div class="form-group">
						<label class="message-label col-md-2" for="message">{{
							'messagePage.create.body' | translate }} </label>
						<div class="col-md-9">
							<textarea class="form-control" rows="5"
								placeholder="{{ 'messagePage.create.body.placeholder' | translate }}"
								id="message" required
								data-ng-model="vm.message"></textarea>
						</div>
					</div>

					<div class="message-send-button">
						<button id="messages_Button_SendMessage" class="btn btn-primary" data-ng-disabled="messageForm.$invalid"
							data-ng-click="vm.sendMsg(); messageForm.$setPristine()"
							data-toggle="modal" data-target="#myModal">
							{{ 'messagePage.create.button.send' | translate }}</button>
					</div>
					<br>
					<div class="col-sm-4"></div>
				</form>

			</div>

		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content" data-ng-cloak>
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">{{ 'messagePage.confirm.header' | translate }}</h4>
				</div>
				<div class="modal-body">
					<div data-ng-show="vm.sentSuccess" class="alert alert-success">
						<strong>{{ 'messagePage.confirm.success' | translate }}</strong>
					</div>
					<div data-ng-show="!vm.sentSuccess" class="alert alert-danger">
						<strong>{{ 'messagePage.confirm.error' | translate }}</strong>
						<hr>
						<p>{{ 'messagePage.confirm.error.info' | translate }}</p>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{
						'messagePage.confirm.button.close' | translate }}</button>
				</div>
			</div>
		</div>
	</div>
</div>