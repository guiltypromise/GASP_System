<div id="inbox" data-ng-controller="inboxMsgController">
<div data-ng-cloak class="panel panel-default">				
	<div class="panel-heading">{{ 'messagePage.inbox.header' | translate }} ({{listRecvdMsgs.length}})</div>
	<div class="panel-body">	
	<uib-accordion id="inbox-accordion" close-others="false">
		<uib-accordion-group is-open="status.open" 
			panel-class="panel-info" 
			data-ng-repeat="msg in listRecvdMsgs | orderBy : '-messageID'" 
			class="inbox-accordion-header inbox-accordion-panel">
			<uib-accordion-heading>
				
				<a class="inbox-accordion-header-anchor" href 
				tabindex="-1" 
				data-ng-click="readMessage( msg )">
				
				<div class="row no-margin-bottom">
					<div class="col-lg-10 col-xs-8">
						<p data-ng-class="{'text-muted': isDisabled}"> 
							{{ 'messagePage.inbox.from' | translate }} {{ msg.sender }} 
						</p>
						<span> {{msg.timestamp}} </span>
					</div>
					<div class="col-lg-1 col-xs-2">
						<span class="label label-success" data-ng-show="msg.isNew">New!</span>	
					</div>
					
					<div class="col-lg-1 col-xs-2">
						<i class="pull-right panel-arrow"
					   		data-ng-class="{'fa fa-chevron-down':!status.open, 'fa fa-chevron-up':status.open}"></i>
					</div>
				</div>
			</a>
			</uib-accordion-heading>
			<h4>{{ msg.subject }}</h4>
			<hr>
			<p>{{msg.message}}</p>
	
		</uib-accordion-group>
	</uib-accordion>	
	</div>
</div>